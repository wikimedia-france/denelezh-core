<?php

/*
rename this file conf.inc.php
*/

define('DB_HOST', 'localhost');
define('DB_USER', 'denelezh');
define('DB_PASSWORD', '');
define('DB_NAME', 'denelezh');

define('SITE_DIR', 'https://www.denelezh.org/');
define('SITE_STATIC_DIR', 'https://www.denelezh.org/static/');
define('SITE_SHORT_DIR', '/envel/denelezh/www');

define('SITE_TITLE', 'Denelezh');

define('GAPS_MINIMUM_THRESHOLD_COUNTRY', 200);
define('GAPS_MINIMUM_THRESHOLD_OCCUPATION', 1000);
define('GAPS_DEFAULT_YEAR_START', 1800);
define('GAPS_DEFAULT_YEAR_END', date('Y'));

?>
