<?php

page::checkUrl(SITE_DIR);
page::set('view', 'index');
page::set('title', 'Gender Gap in Wikimedia projects');

page::set('latest_dump', gaps::dumps()[0]);
page::set('enwiki_latest_data', db::query('SELECT `kpi`.`females`, `kpi`.`males`, `kpi`.`others` FROM `kpi`, `project` WHERE `kpi`.`dump` = \''.page::get('latest_dump').'\' AND `kpi`.`birthyear` = 0 AND `kpi`.`country` = 0 AND `kpi`.`occupation` = 0 AND `kpi`.`project` = `project`.`id` AND `project`.`code` = \'enwiki\'')->fetch_object());

?>