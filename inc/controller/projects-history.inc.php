<?php

page::checkUrl(SITE_DIR.'projects-history/');

page::set('view', 'projects-history');
page::set('title', 'Projects History');

page::set('projects', gaps::getProjectsHistory());

?>