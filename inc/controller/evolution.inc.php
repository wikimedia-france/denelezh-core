<?php

$canonicalUrl = gaps::init('evolution/', array('project' => true, 'country' => true, 'occupation' => true, 'year' => true, 'year_start' => true, 'year_end' => true));
page::checkUrl($canonicalUrl);

page::set('view', 'evolution');
page::set('title', 'Evolution of Gender Gap in '.(!empty(GAPS_PROJECT_ID) ? GAPS_PROJECT_LABEL : 'Wikidata').(!empty(GAPS_FILTERS) ? ' [ '.GAPS_FILTERS.' ]' : ''));

page::set('projects', gaps::projects());
page::set('countries', gaps::countries());
page::set('occupations', gaps::occupations());

page::set('data', gaps::evolution());

?>