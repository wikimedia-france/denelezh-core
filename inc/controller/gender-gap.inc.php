<?php

$canonicalUrl = gaps::init('gender-gap/', array('kpi' => true, 'project' => true, 'country' => true, 'occupation' => true, 'year' => true, 'year_start' => true, 'year_end' => true, 'threshold' => true, 'dump' => true, 'sort' => true));
page::checkUrl($canonicalUrl);

page::set('view', 'gender-gap');
page::set('title', 'Gender Gap in '.(!empty(GAPS_PROJECT_ID) ? GAPS_PROJECT_LABEL : 'Wikidata').(!empty(GAPS_FILTERS) ? ' [ '.GAPS_FILTERS.' ]' : ''));

page::set('projects', gaps::projects());
page::set('countries', gaps::countries());
page::set('occupations', gaps::occupations());

page::set('data', gaps::gendergap());

?>