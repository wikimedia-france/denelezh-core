<?php

class display {
    
    public static function tableHeader($title, $sortable = false, $anchor = null) {
        if ($sortable) {
            echo '<tr>
                <th class="title'.((GAPS_SORT == 'label') ? ' selected' : '').'"><a href="'.gaps::buildLink(array('sort' => 'label', 'anchor' => $anchor)).'">'.$title.' ▴</a></th>
                <th class="data'.((GAPS_SORT == 'total') ? ' selected' : '').'"><a href="'.gaps::buildLink(array('sort' => 'total', 'anchor' => $anchor)).'">Total ▾</a></th>
                <th class="data'.((GAPS_SORT == 'total_with_gender') ? ' selected' : '').'"><a href="'.gaps::buildLink(array('sort' => 'total_with_gender', 'anchor' => $anchor)).'">Total with gender ▾</a></th>
                <th class="data'.((GAPS_SORT == 'females') ? ' selected' : '').'"><a href="'.gaps::buildLink(array('sort' => 'females', 'anchor' => $anchor)).'">Females ▾</a></th>
                <th class="data'.((GAPS_SORT == 'percent_females') ? ' selected' : '').'"><a href="'.gaps::buildLink(array('sort' => 'percent_females', 'anchor' => $anchor)).'">% Fem. ▾</a></th>
                <th class="gap">Gap</th><th class="data'.((GAPS_SORT == 'males') ? ' selected' : '').'"><a href="'.gaps::buildLink(array('sort' => 'males', 'anchor' => $anchor)).'">Males ▾</a></th>
                <th class="data'.((GAPS_SORT == 'percent_males') ? ' selected' : '').'"><a href="'.gaps::buildLink(array('sort' => 'percent_males', 'anchor' => $anchor)).'">% Mal. ▾</a></th>
                <th class="data'.((GAPS_SORT == 'others') ? ' selected' : '').'"><a href="'.gaps::buildLink(array('sort' => 'others', 'anchor' => $anchor)).'">Others ▾</a></th>
                <th class="data'.((GAPS_SORT == 'percent_others') ? ' selected' : '').'"><a href="'.gaps::buildLink(array('sort' => 'percent_others', 'anchor' => $anchor)).'">% Oth. ▾</a></th>
            </tr>'."\n";
        } else {
            echo '<tr><th class="title">'.$title.'</th><th class="data">Total</th><th class="data">Total with gender</th><th class="data">Females</th><th class="data">% Females</th><th class="gap">Gap</th><th class="data">Males</th><th class="data">% Males</th><th class="data">Others</th><th class="data">% Others</th></tr>'."\n";
        }
    }
    
    public static function tableRow($title, $humans, $females, $males, $others) {
        $humans_with_gender = $females + $males + $others;
        echo '<tr><th class="row"><span title="'.trim(strip_tags($title)).'">'.$title.'</span></th><td class="data">'.number_format($humans).'</td><td class="data">'.number_format($humans_with_gender).'</td><td class="data">'.number_format($females).'</td><td class="data">'.self::percent($humans_with_gender, $females).'</td><td class="gap">';
        if ($humans_with_gender >= GAPS_PEOPLE_THRESHOLD_VALUE) {
            echo '<div style="width: '.number_format(100 / $humans_with_gender * $females, 3).'%;" class="female"></div><div style="width: '.number_format(100 / $humans_with_gender * $males, 3).'%;" class="male"></div>';
        } else {
            echo '<em>total under the threshold of '.GAPS_PEOPLE_THRESHOLD_VALUE.' people</em>';
        }
        echo '</td><td class="data">'.number_format($males).'</td><td class="data">'.self::percent($humans_with_gender, $males).'</td><td class="data">'.number_format($others).'</td><td class="data">'.self::percent($humans_with_gender, $others).'</td></tr>'."\n";
    }
    
    public static function tableEmptyTableRow($message) {
        echo '<tr><th class="row"></th><td class="data"></td><td class="data"></td><td class="data"></td><td class="data"></td><td class="gap"><em>'.htmlentities($message).'</em></td><td class="data"></td><td class="data"></td><td class="data"></td><td class="data"></td></tr>'."\n";
    }
    
    public static function buildLabel($id, $label) {
        if (!empty($label)) {
            return htmlentities($label);
        } else {
            return 'Q'.$id;
        }
    }
    
    public static function percent($total, $share) {
        if ($total == 0) {
            return 'N/A';
        }
        return number_format(100 / $total * $share, 3).' %';
    }
    
    public static function form_project() {
        $r = '<label for="form_project">Wikimedia project</label> <select id="form_project" name="project"><option value="">Any</option>';
        foreach (page::get('projects') as $project) {
            $r .= '<option value="'.$project->code.'"';
            if ($project->code === GAPS_PROJECT_CODE) {
                $r .= ' selected="selected"';
            }
            $r .= '>'.htmlentities($project->label).' ('.$project->code.')</option>'."\n";
        }
        $r .= '</select>';
        if (GAPS_PROJECT_ID != 0) {
            $r .= ' <a href="'.gaps::buildLink(array('project' => '')).'" title="remove this filter"><img src="'.SITE_STATIC_DIR.'img/cross.png" alt="" /></a>';
        }
        $r .= '<br />';
        return $r;
    }
    
    public static function form_country() {
        $r = '<label for="form_country">Country of citizenship</label> <select id="form_country" name="country"><option value="0">Any</option>';
        foreach (page::get('countries') as $country) {
            $r .= '<option value="'.$country->id.'"';
            if ($country->id === GAPS_COUNTRY) {
                $r .= ' selected="selected"';
            }
            $r .= '>'.htmlentities($country->label).' (Q'.$country->id.')</option>'."\n";
        }
        $r .= '</select>';
        if (GAPS_COUNTRY != 0) {
            $r .= ' <a href="'.gaps::buildLink(array('country' => '')).'" title="remove this filter"><img src="'.SITE_STATIC_DIR.'img/cross.png" alt="" /></a>';
        }
        $r .= '<br />';
        return $r;
    }
    
    public static function form_occupation() {
        $r = '<label for="form_occupation">Occupation</label> <select id="form_occupation" name="occupation"><option value="0">Any</option>';
        foreach (page::get('occupations') as $occupation) {
            $r .= '<option value="'.$occupation->id.'"';
            if ($occupation->id === GAPS_OCCUPATION) {
                $r .= ' selected="selected"';
            } $r .= '>'.htmlentities($occupation->label).' (Q'.$occupation->id.')</option>'."\n";
        }
        $r .= '</select>';
        if (GAPS_OCCUPATION != 0) {
            $r .= ' <a href="'.gaps::buildLink(array('occupation' => '')).'" title="remove this filter"><img src="'.SITE_STATIC_DIR.'img/cross.png" alt="" /></a>';
        }
        $r .= '<br />';
        return $r;
    }
    
    public static function form_year() {
        $r = '<label>Year of birth</label> <input type="radio" id="form_year_any" name="year" value="any"';
        if (GAPS_YEAR === 'any') {
            $r .= ' checked="checked"';
        }
        $r .= ' /> <label for="form_year_any" class="rightLabel">Any</label><br /><label></label> <input type="radio" id="form_year_specified" name="year" value="specified"';
        if (GAPS_YEAR === 'specified') {
            $r .= ' checked="checked"';
        }
        $r .= ' /> <label for="year_start" class="rightLabel">From</label> <input id="year_start" name="year_start" type="text" value="'.GAPS_YEAR_START.'" /> <label for="year_end" class="rightLabel">to</label> <input id="year_end" name="year_end" type="text" value="'.GAPS_YEAR_END.'" />';
        if (GAPS_YEAR === 'specified') {
            $r .= ' <a href="'.gaps::buildLink(array('year' => '', 'year_start' => '', 'year_end' => '')).'" title="remove this filter"><img src="'.SITE_STATIC_DIR.'img/cross.png" alt="" /></a>';
        }
        $r .= '<br />';
        return $r;
    }
    
}

?>