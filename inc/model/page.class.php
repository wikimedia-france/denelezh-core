<?php

class page {
	
	private static $data = array();
	
	public static function get($key) {
		if (!isset(self::$data[$key])) {
			return null;
		} else {
			return self::$data[$key];
		}
	}
	
	public static function set($key, $value) {
		self::$data[$key] = $value;
	}
	
	public static function checkUrl($url) {
		if ($url !== $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']) {
            page::redirectTo($url);
		}
	}
    
	public static function redirectTo($url) {
		header('Status: 301 Moved Permanently', false, 301);
		header('Location: '.$url);
		exit;
	}
    
}

?>