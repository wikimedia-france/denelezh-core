<?php

class gaps {
    
    private static $dumps = null;
    private static $kpis = array('humans' => '', 'with_sitelink' => '_with_sitelink', 'sitelinks' => '_sitelinks');
    private static $sorts = null;
    private static $isDefaultFilters = true;
    private static $page = '';
    private static $usedFilters = array();
    
    public static function dumps() {
        if (self::$dumps === null) {
            self::$dumps = array();
            $res = db::query('SELECT `date` FROM `dump` WHERE `step` = 1 ORDER BY `date` DESC');
            while ($dump = $res->fetch_object()) {
                self::$dumps[] = $dump->date;
            }
        }
        return self::$dumps;
    }
    
    public static function kpis() {
        return self::$kpis;
    }
    
    public static function projects() {
        $r = array();
        $res = db::query('SELECT `code`, `label` FROM `project` ORDER BY `label` ASC, `code` ASC');
        while ($project = $res->fetch_object()) {
            $r[] = $project;
        }
        return $r;
    }
    
    public static function countries() {
        $r = array();
        $res = db::query('SELECT `id`, `label` FROM `country` ORDER BY `label` ASC');
        while ($country = $res->fetch_object()) {
            $r[] = $country;
        }
        return $r;
    }
    
    public static function occupations() {
        $r = array();
        $res = db::query('SELECT `id`, `label` FROM `occupation` ORDER BY `label` ASC');
        while ($occupation = $res->fetch_object()) {
            $r[] = $occupation;
        }
        return $r;
    }
    
    public static function sorts() {
        if (self::$sorts === null) {
            self::$sorts = array(
                'label' => '`label` ASC',
                'total' => 'SUM(`kpi`.`humans'.self::kpis()[GAPS_KPI].'`) DESC, `label` ASC',
                'total_with_gender' => 'SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `label` ASC',
                'females' => 'SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `label` ASC',
                'percent_females' => '100 * SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'`) / SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `label` ASC',
                'males' => 'SUM(`kpi`.`males'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `label` ASC',
                'percent_males' => '100 * SUM(`kpi`.`males'.self::kpis()[GAPS_KPI].'`) / SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`males'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `label` ASC',
                'others' => 'SUM(`kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `label` ASC',
                'percent_others' => '100 * SUM(`kpi`.`others'.self::kpis()[GAPS_KPI].'`) / SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `label` ASC',
                // project are sorted by code -_-
                'code_label' => '`code` ASC',
                'code_total' => 'SUM(`kpi`.`humans'.self::kpis()[GAPS_KPI].'`) DESC, `code` ASC',
                'code_total_with_gender' => 'SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `code` ASC',
                'code_females' => 'SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `code` ASC',
                'code_percent_females' => '100 * SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'`) / SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `code` ASC',
                'code_males' => 'SUM(`kpi`.`males'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `code` ASC',
                'code_percent_males' => '100 * SUM(`kpi`.`males'.self::kpis()[GAPS_KPI].'`) / SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`males'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `code` ASC',
                'code_others' => 'SUM(`kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `code` ASC',
                'code_percent_others' => '100 * SUM(`kpi`.`others'.self::kpis()[GAPS_KPI].'`) / SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, SUM(`kpi`.`females'.self::kpis()[GAPS_KPI].'` + `kpi`.`males'.self::kpis()[GAPS_KPI].'` + `kpi`.`others'.self::kpis()[GAPS_KPI].'`) DESC, `code` ASC',
            );
        }
        return self::$sorts;
    }
    
    public static function isDefaultFilters() {
        return self::$isDefaultFilters;
    }
    
    public static function init($page = '', $usedFilters = array()) {
        
        self::$page = $page;
        self::$usedFilters = $usedFilters;
        
        $canonicalUrlValues = array();
        
        if (isset($usedFilters['kpi']) && $usedFilters['kpi']) {
            if (!empty($_GET['kpi']) && isset(self::kpis()[$_GET['kpi']])) {
                define('GAPS_KPI', $_GET['kpi']);
                if ($_GET['kpi'] !== 'with_sitelink') {
                    define('GAPS_KPI_LINK', $_GET['kpi']);
                    self::$isDefaultFilters = false;
                    $canonicalUrlValues[] = 'kpi='.GAPS_KPI;
                    page::set('index', false);
                } else {
                    define('GAPS_KPI_LINK', '');
                }
            } else {
                define('GAPS_KPI', 'with_sitelink');
                define('GAPS_KPI_LINK', '');
            }
        }
        
        if (isset($usedFilters['project']) && $usedFilters['project']) {
            if (!empty($_GET['project']) && preg_match('/^[a-z_]+$/', $_GET['project'])) {
                $res = db::query('SELECT `id`, `label` FROM `project` WHERE `code` = \''.$_GET['project'].'\'');
                if ($res->num_rows === 1) {
                    $project = $res->fetch_object();
                    define('GAPS_PROJECT_ID', $project->id);
                    define('GAPS_PROJECT_CODE', $_GET['project']);
                    if (!empty($project->label)) {
                        define('GAPS_PROJECT_LABEL', $project->label);
                    } else {
                        define('GAPS_PROJECT_LABEL', $_GET['project']);
                    }
                    self::$isDefaultFilters = false;
                    $canonicalUrlValues[] = 'project='.GAPS_PROJECT_CODE;
                } else {
                    define('GAPS_PROJECT_ID', 0);
                    define('GAPS_PROJECT_CODE', '');
                    define('GAPS_PROJECT_LABEL', '');
                }
            } else {
                define('GAPS_PROJECT_ID', 0);
                define('GAPS_PROJECT_CODE', '');
                define('GAPS_PROJECT_LABEL', '');
            }
        }
        
        if (isset($usedFilters['country']) && $usedFilters['country']) {
            if (!empty($_GET['country']) && preg_match('/^[1-9][0-9]*$/', $_GET['country'])) {
                $res = db::query('SELECT `label` FROM `country` WHERE `id` = '.$_GET['country']);
                if ($res->num_rows === 1) {
                    define('GAPS_COUNTRY', $_GET['country']);
                    $label = $res->fetch_object()->label;
                    if (!empty($label)) {
                        define('GAPS_COUNTRY_LABEL', $label);
                    } else {
                        define('GAPS_COUNTRY_LABEL', 'Q'.$_GET['country']);
                    }
                    self::$isDefaultFilters = false;
                    $canonicalUrlValues[] = 'country='.GAPS_COUNTRY;
                } else {
                    define('GAPS_COUNTRY', 0);
                }
            } else {
                define('GAPS_COUNTRY', 0);
            }
        }
        
        if (isset($usedFilters['occupation']) && $usedFilters['occupation']) {
            if (!empty($_GET['occupation']) && preg_match('/^[1-9][0-9]*$/', $_GET['occupation'])) {
                $res = db::query('SELECT `label` FROM `occupation` WHERE `id` = '.$_GET['occupation']);
                if ($res->num_rows === 1) {
                    define('GAPS_OCCUPATION', $_GET['occupation']);
                    $label = $res->fetch_object()->label;
                    if (!empty($label)) {
                        define('GAPS_OCCUPATION_LABEL', $label);
                    } else {
                        define('GAPS_OCCUPATION_LABEL', 'Q'.$_GET['occupation']);
                    }
                    self::$isDefaultFilters = false;
                    $canonicalUrlValues[] = 'occupation='.GAPS_OCCUPATION;
                } else {
                    define('GAPS_OCCUPATION', 0);
                }
            } else {
                define('GAPS_OCCUPATION', 0);
            }
        }
        
        if (isset($usedFilters['year']) && $usedFilters['year']) {
            if (isset($_GET['year']) && ($_GET['year'] === 'specified')) {
                define('GAPS_YEAR', $_GET['year']);
                if (isset($_GET['year_start']) && preg_match('/^-?[0-9]+$/', $_GET['year_start'])) {
                    define('GAPS_YEAR_START', $_GET['year_start']);
                } else {
                    define('GAPS_YEAR_START', GAPS_DEFAULT_YEAR_START);
                }
                if (isset($_GET['year_end']) && preg_match('/^-?[0-9]+$/', $_GET['year_end'])) {
                    define('GAPS_YEAR_END', $_GET['year_end']);
                } else {
                    define('GAPS_YEAR_END', GAPS_DEFAULT_YEAR_END);
                }
                define('GAPS_YEAR_LINK', GAPS_YEAR);
                define('GAPS_YEAR_START_LINK', GAPS_YEAR_START);
                define('GAPS_YEAR_END_LINK', GAPS_YEAR_END);
                define('GAPS_YEAR_LABEL', GAPS_YEAR_START.' to '.GAPS_YEAR_END);
                self::$isDefaultFilters = false;
                $canonicalUrlValues[] = 'year='.GAPS_YEAR;
                $canonicalUrlValues[] = 'year_start='.GAPS_YEAR_START;
                $canonicalUrlValues[] = 'year_end='.GAPS_YEAR_END;
                page::set('index', false);
            } else {
                define('GAPS_YEAR', 'any');
                define('GAPS_YEAR_START', GAPS_DEFAULT_YEAR_START);
                define('GAPS_YEAR_END', GAPS_DEFAULT_YEAR_END);
                define('GAPS_YEAR_LINK', '');
                define('GAPS_YEAR_START_LINK', '');
                define('GAPS_YEAR_END_LINK', '');
                define('GAPS_YEAR_LABEL', '');
            }
        }
        
        // must be defined, even if not $usedFilters['year'] (the goal is that SQL queries use indexing)
        if (defined('GAPS_YEAR')) {
            if (GAPS_YEAR === 'specified') {
                define('GAPS_FROM', ', `year`');
                define('GAPS_WHERE', '`kpi`.`birthyear` = `year`.`id` AND `year`.`year` >= '.GAPS_YEAR_START.' AND `year`.`year` <= '.GAPS_YEAR_END);
            } else {
                define('GAPS_FROM', '');
                define('GAPS_WHERE', '`kpi`.`birthyear` = 0');
            }
        }
        
        if (isset($usedFilters['dump']) && $usedFilters['dump']) {
            if (!empty($_GET['dump']) && in_array($_GET['dump'], self::dumps())) {
                define('GAPS_DUMPDATE', $_GET['dump']);
                define('GAPS_DUMPDATE_LABEL', GAPS_DUMPDATE);
                define('GAPS_DUMPDATE_LINK', GAPS_DUMPDATE);
                self::$isDefaultFilters = false;
                $canonicalUrlValues[] = 'dump='.GAPS_DUMPDATE;
                page::set('index', false);
            } else {
                define('GAPS_DUMPDATE', self::dumps()[0]);
                define('GAPS_DUMPDATE_LABEL', 'latest');
                define('GAPS_DUMPDATE_LINK', '');
            }
        }
        
        if (isset($usedFilters['threshold']) && $usedFilters['threshold']) {
            if (!empty($_GET['threshold']) && preg_match('/^[1-9][0-9]*$/', $_GET['threshold'])) {
                define('GAPS_PEOPLE_THRESHOLD', $_GET['threshold']);
                define('GAPS_PEOPLE_THRESHOLD_VALUE', GAPS_PEOPLE_THRESHOLD);
                define('GAPS_PEOPLE_THRESHOLD_LINK', GAPS_PEOPLE_THRESHOLD);
                self::$isDefaultFilters = false;
                $canonicalUrlValues[] = 'threshold='.GAPS_PEOPLE_THRESHOLD;
                page::set('index', false);
            } else {
                define('GAPS_PEOPLE_THRESHOLD', 'auto');
                $max = db::query('SELECT SUM(`humans'.self::kpis()[GAPS_KPI].'`) AS `humans` FROM `kpi`'.GAPS_FROM.' WHERE '.GAPS_WHERE.' AND `dump` = \''.GAPS_DUMPDATE.'\' AND `country` = '.GAPS_COUNTRY.' AND `occupation` = '.GAPS_OCCUPATION.' AND `project` = '.GAPS_PROJECT_ID)->fetch_object()->humans;
                define('GAPS_PEOPLE_THRESHOLD_VALUE', max(min(20000, round($max / 100)), 100));
                define('GAPS_PEOPLE_THRESHOLD_LINK', '');
            }
        }
        
        if (isset($usedFilters['sort']) && $usedFilters['sort']) {
            if (!empty($_GET['sort']) && isset(self::sorts()[$_GET['sort']])) {
                define('GAPS_SORT', $_GET['sort']);
                if ($_GET['sort'] !== 'total') {
                    define('GAPS_SORT_LINK', $_GET['sort']);
                    self::$isDefaultFilters = false;
                    $canonicalUrlValues[] = 'sort='.GAPS_SORT;
                    page::set('index', false);
                } else {
                    define('GAPS_SORT_LINK', '');
                }
            } else {
                define('GAPS_SORT', 'total');
                define('GAPS_SORT_LINK', '');
            }
        }
        
        $filters = array();
        if (defined('GAPS_COUNTRY') && !empty(GAPS_COUNTRY)) {
            $filters[] = htmlentities(GAPS_COUNTRY_LABEL);
        }
        if (defined('GAPS_OCCUPATION') && !empty(GAPS_OCCUPATION)) {
            $filters[] = htmlentities(GAPS_OCCUPATION_LABEL);
        }
        if (defined('GAPS_YEAR_LABEL') && !empty(GAPS_YEAR_LABEL)) {
            $filters[] = htmlentities(GAPS_YEAR_LABEL);
        }
        if (defined('GAPS_DUMPDATE') && !empty(GAPS_DUMPDATE) && (GAPS_DUMPDATE !== self::dumps()[0])) {
            $filters[] = htmlentities(GAPS_DUMPDATE);
        }
        if (count($filters) >= 1) {
            define('GAPS_FILTERS', implode(' | ', $filters));
        } else {
            define('GAPS_FILTERS', '');
        }
        
        $canonicalUrl = SITE_DIR.self::$page.(!empty($canonicalUrlValues) ? '?'.implode('&', $canonicalUrlValues) : '');
        return $canonicalUrl;
        
    }
    
    public static function gendergap() {
        
        $data['global'] = db::query('SELECT `humans'.gaps::kpis()[GAPS_KPI].'` AS `humans`, `females'.gaps::kpis()[GAPS_KPI].'` AS `females`, `males'.gaps::kpis()[GAPS_KPI].'` AS `males`, `others'.gaps::kpis()[GAPS_KPI].'` AS `others` FROM `kpi` WHERE `dump` = \''.GAPS_DUMPDATE.'\' AND `country` = 0 AND `birthyear` = 0 AND `occupation` = 0 AND `project` = 0')->fetch_object();
        
        if (!empty(GAPS_PROJECT_LABEL)) {
            $data['project'] = self::getGenderGap('SELECT `humans'.gaps::kpis()[GAPS_KPI].'` AS `humans`, `females'.gaps::kpis()[GAPS_KPI].'` AS `females`, `males'.gaps::kpis()[GAPS_KPI].'` AS `males`, `others'.gaps::kpis()[GAPS_KPI].'` AS `others` FROM `kpi` WHERE `dump` = \''.GAPS_DUMPDATE.'\' AND `country` = 0 AND `birthyear` = 0 AND `occupation` = 0 AND `project` = '.GAPS_PROJECT_ID);
        }
        
        if (!empty(GAPS_YEAR_LABEL)) {
            $data['year'] = self::getGenderGap('SELECT SUM(`humans'.gaps::kpis()[GAPS_KPI].'`) AS `humans`, SUM(`females'.gaps::kpis()[GAPS_KPI].'`) AS `females`, SUM(`males'.gaps::kpis()[GAPS_KPI].'`) AS `males`, SUM(`others'.gaps::kpis()[GAPS_KPI].'`) AS `others` FROM `kpi`'.GAPS_FROM.' WHERE '.GAPS_WHERE.' AND `dump` = \''.GAPS_DUMPDATE.'\' AND `country` = 0 AND `occupation` = 0 AND `project` = '.GAPS_PROJECT_ID);
        }
        
        if (!empty(GAPS_COUNTRY)) {
            $data['country'] = self::getGenderGap('SELECT `humans'.gaps::kpis()[GAPS_KPI].'` AS `humans`, `females'.gaps::kpis()[GAPS_KPI].'` AS `females`, `males'.gaps::kpis()[GAPS_KPI].'` AS `males`, `others'.gaps::kpis()[GAPS_KPI].'` AS `others` FROM `kpi` WHERE `dump` = \''.GAPS_DUMPDATE.'\' AND `country` = '.GAPS_COUNTRY.' AND `birthyear` = 0 AND `occupation` = 0 AND `project` = '.GAPS_PROJECT_ID);
        }
        
        if (!empty(GAPS_OCCUPATION)) {
            $data['occupation'] = self::getGenderGap('SELECT `humans'.gaps::kpis()[GAPS_KPI].'` AS `humans`, `females'.gaps::kpis()[GAPS_KPI].'` AS `females`, `males'.gaps::kpis()[GAPS_KPI].'` AS `males`, `others'.gaps::kpis()[GAPS_KPI].'` AS `others` FROM `kpi` WHERE `dump` = \''.GAPS_DUMPDATE.'\' AND `country` = 0 AND `birthyear` = 0 AND `occupation` = '.GAPS_OCCUPATION.' AND `project` = '.GAPS_PROJECT_ID);
        }
        
        if (!empty(GAPS_YEAR_LABEL) && !empty(GAPS_COUNTRY)) {
            $data['year_country'] = self::getGenderGap('SELECT SUM(`humans'.gaps::kpis()[GAPS_KPI].'`) AS `humans`, SUM(`females'.gaps::kpis()[GAPS_KPI].'`) AS `females`, SUM(`males'.gaps::kpis()[GAPS_KPI].'`) AS `males`, SUM(`others'.gaps::kpis()[GAPS_KPI].'`) AS `others` FROM `kpi`'.GAPS_FROM.' WHERE '.GAPS_WHERE.' AND `dump` = \''.GAPS_DUMPDATE.'\' AND `country` = '.GAPS_COUNTRY.' AND `occupation` = 0 AND `project` = '.GAPS_PROJECT_ID);
        }
        
        if (!empty(GAPS_YEAR_LABEL) && !empty(GAPS_OCCUPATION)) {
            $data['year_occupation'] = self::getGenderGap('SELECT SUM(`humans'.gaps::kpis()[GAPS_KPI].'`) AS `humans`, SUM(`females'.gaps::kpis()[GAPS_KPI].'`) AS `females`, SUM(`males'.gaps::kpis()[GAPS_KPI].'`) AS `males`, SUM(`others'.gaps::kpis()[GAPS_KPI].'`) AS `others` FROM `kpi`'.GAPS_FROM.' WHERE '.GAPS_WHERE.' AND `dump` = \''.GAPS_DUMPDATE.'\' AND `country` = 0 AND `occupation` = '.GAPS_OCCUPATION.' AND `project` = '.GAPS_PROJECT_ID);
        }
        
        if (!empty(GAPS_COUNTRY) && !empty(GAPS_OCCUPATION)) {
            $data['country_occupation'] = self::getGenderGap('SELECT SUM(`humans'.gaps::kpis()[GAPS_KPI].'`) AS `humans`, SUM(`females'.gaps::kpis()[GAPS_KPI].'`) AS `females`, SUM(`males'.gaps::kpis()[GAPS_KPI].'`) AS `males`, SUM(`others'.gaps::kpis()[GAPS_KPI].'`) AS `others` FROM `kpi` WHERE `dump` = \''.GAPS_DUMPDATE.'\' AND `country` = '.GAPS_COUNTRY.' AND `birthyear` = 0 AND `occupation` = '.GAPS_OCCUPATION.' AND `project` = '.GAPS_PROJECT_ID);
        }
        
        if (!empty(GAPS_YEAR_LABEL) && !empty(GAPS_COUNTRY) && !empty(GAPS_OCCUPATION)) {
            $data['year_country_occupation'] = self::getGenderGap('SELECT SUM(`humans'.gaps::kpis()[GAPS_KPI].'`) AS `humans`, SUM(`females'.gaps::kpis()[GAPS_KPI].'`) AS `females`, SUM(`males'.gaps::kpis()[GAPS_KPI].'`) AS `males`, SUM(`others'.gaps::kpis()[GAPS_KPI].'`) AS `others` FROM `kpi`'.GAPS_FROM.' WHERE '.GAPS_WHERE.' AND `dump` = \''.GAPS_DUMPDATE.'\' AND `country` = '.GAPS_COUNTRY.' AND `occupation` = '.GAPS_OCCUPATION.' AND `project` = '.GAPS_PROJECT_ID);
        }
        
        if (empty(GAPS_YEAR_LABEL)) {
            $data['no_year'] = array();
            for ($year = GAPS_DEFAULT_YEAR_START; $year < GAPS_DEFAULT_YEAR_END; $year += 10) {
                $data['no_year'][$year] = self::getGenderGap('SELECT SUM(`humans'.gaps::kpis()[GAPS_KPI].'`) AS `humans`, SUM(`females'.gaps::kpis()[GAPS_KPI].'`) AS `females`, SUM(`males'.gaps::kpis()[GAPS_KPI].'`) AS `males`, SUM(`others'.gaps::kpis()[GAPS_KPI].'`) AS `others` FROM `kpi`, `year` WHERE `dump` = \''.GAPS_DUMPDATE.'\' AND `kpi`.`birthyear` = `year`.`id` AND `year`.`year` >= '.$year.' AND `year`.`year` <= '.($year + 9).' AND `country` = '.GAPS_COUNTRY.' AND `occupation` = '.GAPS_OCCUPATION.' AND `project` = '.GAPS_PROJECT_ID);
            }
        }
        
        if (empty(GAPS_COUNTRY)) {
            $data['no_country'] = array();
            $res = db::query('SELECT `country`.`id`, `country`.`label`, SUM(`humans'.gaps::kpis()[GAPS_KPI].'`) AS `humans`, SUM(`kpi`.`females'.gaps::kpis()[GAPS_KPI].'`) AS `females`, SUM(`kpi`.`males'.gaps::kpis()[GAPS_KPI].'`) AS `males`, SUM(`kpi`.`others'.gaps::kpis()[GAPS_KPI].'`) AS `others` FROM `kpi`, `country`'.GAPS_FROM.' WHERE '.GAPS_WHERE.' AND `kpi`.`country` = `country`.`id` AND `kpi`.`dump` = \''.GAPS_DUMPDATE.'\' AND `kpi`.`occupation` = '.GAPS_OCCUPATION.' AND `kpi`.`project` = '.GAPS_PROJECT_ID.' GROUP BY `country`.`id` HAVING SUM(`kpi`.`humans'.gaps::kpis()[GAPS_KPI].'`) >= '.GAPS_PEOPLE_THRESHOLD_VALUE.' ORDER BY '.gaps::sorts()[GAPS_SORT]);
            while ($row = $res->fetch_object()) {
                $data['no_country'][] = $row;
            }
        }
        
        if (empty(GAPS_OCCUPATION)) {
            $data['no_occupation'] = array();
            $res = db::query('SELECT `occupation`.`id`, `occupation`.`label`, SUM(`humans'.gaps::kpis()[GAPS_KPI].'`) AS `humans`, SUM(`kpi`.`females'.gaps::kpis()[GAPS_KPI].'`) AS `females`, SUM(`kpi`.`males'.gaps::kpis()[GAPS_KPI].'`) AS `males`, SUM(`kpi`.`others'.gaps::kpis()[GAPS_KPI].'`) AS `others` FROM `kpi`, `occupation`'.GAPS_FROM.' WHERE '.GAPS_WHERE.' AND `kpi`.`occupation` = `occupation`.`id` AND `kpi`.`dump` = \''.GAPS_DUMPDATE.'\' AND `kpi`.`country` = '.GAPS_COUNTRY.' AND `kpi`.`project` = '.GAPS_PROJECT_ID.' GROUP BY `occupation`.`id` HAVING SUM(`kpi`.`humans'.gaps::kpis()[GAPS_KPI].'`) >= '.GAPS_PEOPLE_THRESHOLD_VALUE.' ORDER BY '.gaps::sorts()[GAPS_SORT]);
            while ($row = $res->fetch_object()) {
                $data['no_occupation'][] = $row;
            }
        }
        
        if (empty(GAPS_PROJECT_LABEL)) {
            $data['no_project'] = array();
            $res = db::query('SELECT `project`.`code`, `project`.`type`, `project`.`label`, `project`.`url`, SUM(`humans'.gaps::kpis()[GAPS_KPI].'`) AS `humans`, SUM(`kpi`.`females'.gaps::kpis()[GAPS_KPI].'`) AS `females`, SUM(`kpi`.`males'.gaps::kpis()[GAPS_KPI].'`) AS `males`, SUM(`kpi`.`others'.gaps::kpis()[GAPS_KPI].'`) AS `others` FROM `kpi`, `project`'.GAPS_FROM.' WHERE '.GAPS_WHERE.' AND `kpi`.`project` = `project`.`id` AND `kpi`.`dump` = \''.GAPS_DUMPDATE.'\' AND `kpi`.`country` = '.GAPS_COUNTRY.' AND `kpi`.`occupation` = '.GAPS_OCCUPATION.' GROUP BY `project`.`code` HAVING SUM(`kpi`.`humans'.gaps::kpis()[GAPS_KPI].'`) >= '.GAPS_PEOPLE_THRESHOLD_VALUE.' ORDER BY '.gaps::sorts()['code_'.GAPS_SORT]);
            while ($row = $res->fetch_object()) {
                $data['no_project'][] = $row;
            }
        }
        
        return $data;
        
    }
    
    private static function getGenderGap($query) {
        $res = db::query($query);
        if ($res->num_rows === 1) {
            return $res->fetch_object();
        }
        return (object) array('humans' => 0, 'females' => 0, 'males' => 0, 'others' => 0);
    }
    
    public static function evolution() {
        // don't use gaps::dumps() as data can be missing
        $data['dumps'] = array();
        $data['dumps_der'] = array();
        $previousDump = null;
        $affixes = array('all' => '', 'with_sitelink' => '_with_sitelink', 'sitelinks' => '_sitelinks');
        $res = db::query('SELECT `dump`, SUM(`humans`) AS `humans`, SUM(`females`) AS `females`, SUM(`males`) AS `males`, SUM(`others`) AS `others`, SUM(`humans_with_sitelink`) AS `humans_with_sitelink`, SUM(`females_with_sitelink`) AS `females_with_sitelink`, SUM(`males_with_sitelink`) AS `males_with_sitelink`, SUM(`others_with_sitelink`) AS `others_with_sitelink`, SUM(`humans_sitelinks`) AS `humans_sitelinks`, SUM(`females_sitelinks`) AS `females_sitelinks`, SUM(`males_sitelinks`) AS `males_sitelinks`, SUM(`others_sitelinks`) AS `others_sitelinks` FROM `kpi`'.GAPS_FROM.' WHERE '.GAPS_WHERE.' AND `dump` IN (\''.implode('\', \'', gaps::dumps()).'\') AND `country` = '.GAPS_COUNTRY.' AND `occupation` = '.GAPS_OCCUPATION.' AND `project` = '.GAPS_PROJECT_ID.' GROUP BY `dump` ORDER BY `dump` ASC');
        while ($row = $res->fetch_object()) {
            $data['dumps'][] = $row->dump;
            if ($previousDump != null) {
                $data['dumps_der'][] = $row->dump;
            }
            foreach ($affixes as $key => $affixe) {
                $data['humans'][$key][$row->dump] = $row->{'humans'.$affixe};
                $data['females'][$key][$row->dump] = $row->{'females'.$affixe};
                $data['males'][$key][$row->dump] = $row->{'males'.$affixe};
                $data['others'][$key][$row->dump] = $row->{'others'.$affixe};
                $data['sum'][$key][$row->dump] = $row->{'females'.$affixe} + $row->{'males'.$affixe} + $row->{'others'.$affixe};
                if ($data['sum'][$key][$row->dump] == 0) {
                    $data['females_percent'][$key][$row->dump] = 0;
                    $data['males_percent'][$key][$row->dump] = 0;
                    $data['others_percent'][$key][$row->dump] = 0;
                    if ($previousDump != null) {
                        if ($data['sum'][$key][$previousDump] == 0) {
                            $data['females_percent_der'][$key][$row->dump] = 0;
                            $data['males_percent_der'][$key][$row->dump] = 0;
                            $data['others_percent_der'][$key][$row->dump] = 0;
                        } else {
                            $data['females_percent_der'][$key][$row->dump] = number_format(-(100.0 / $data['sum'][$key][$previousDump] * $row->{'females'.$affixe}) / $diff * 7, 3);
                            $data['males_percent_der'][$key][$row->dump] = number_format(-(100.0 / $data['sum'][$key][$previousDump] * $row->{'males'.$affixe}) / $diff * 7, 3);
                            $data['others_percent_der'][$key][$row->dump] = number_format(-(100.0 / $data['sum'][$key][$previousDump] * $row->{'others'.$affixe}) / $diff * 7, 3);
                        }
                    }
                } else {
                    $data['females_percent'][$key][$row->dump] = number_format(100.0 / $data['sum'][$key][$row->dump] * $row->{'females'.$affixe}, 3);
                    $data['males_percent'][$key][$row->dump] = number_format(100.0 / $data['sum'][$key][$row->dump] * $row->{'males'.$affixe}, 3);
                    $data['others_percent'][$key][$row->dump] = number_format(100.0 / $data['sum'][$key][$row->dump] * $row->{'others'.$affixe}, 3);
                    if ($previousDump != null) {
                        if ($data['sum'][$key][$previousDump] == 0) {
                            $data['females_percent_der'][$key][$row->dump] = number_format((100.0 / $data['sum'][$key][$row->dump] * $row->{'females'.$affixe}) / $diff * 7, 3);
                            $data['males_percent_der'][$key][$row->dump] = number_format((100.0 / $data['sum'][$key][$row->dump] * $row->{'males'.$affixe}) / $diff * 7, 3);
                            $data['others_percent_der'][$key][$row->dump] = number_format((100.0 / $data['sum'][$key][$row->dump] * $row->{'others'.$affixe}) / $diff * 7, 3);
                        } else {
                            $data['females_percent_der'][$key][$row->dump] = number_format((100.0 / $data['sum'][$key][$row->dump] * $row->{'females'.$affixe}) - (100.0 / $data['sum'][$key][$previousDump] * $data['females'][$key][$previousDump]), 3);
                            $data['males_percent_der'][$key][$row->dump] = number_format((100.0 / $data['sum'][$key][$row->dump] * $row->{'males'.$affixe}) - (100.0 / $data['sum'][$key][$previousDump] * $data['males'][$key][$previousDump]), 3);
                            $data['others_percent_der'][$key][$row->dump] = number_format((100.0 / $data['sum'][$key][$row->dump] * $row->{'others'.$affixe}) - (100.0 / $data['sum'][$key][$previousDump] * $data['others'][$key][$previousDump]), 3);
                        }
                    }
                }
                if ($previousDump != null) {
                    $diff = (new DateTime($row->dump))->diff(new DateTime($previousDump))->format("%a");
                    $data['humans_der'][$key][$row->dump] = round(($row->{'humans'.$affixe} - $data['humans'][$key][$previousDump]) / $diff * 7, 1);
                    $data['females_der'][$key][$row->dump] = round(($row->{'females'.$affixe} - $data['females'][$key][$previousDump]) / $diff * 7, 1);
                    $data['males_der'][$key][$row->dump] = round(($row->{'males'.$affixe} - $data['males'][$key][$previousDump]) / $diff * 7, 1);
                    $data['others_der'][$key][$row->dump] = round(($row->{'others'.$affixe} - $data['others'][$key][$previousDump]) / $diff * 7, 1);
                    $data['sum_der'][$key][$row->dump] = round(($row->{'females'.$affixe} + $row->{'males'.$affixe} + $row->{'others'.$affixe} - $data['females'][$key][$previousDump] - $data['males'][$key][$previousDump] - $data['others'][$key][$previousDump]) / $diff * 7, 1);
                }
            }
            $previousDump = $row->dump;
        }
        return $data;
    }
    
    public static function getProjectsHistory() {
        $projects = array();
        foreach (array_reverse(self::dumps()) as $dump) {
            $res = db::query('SELECT `project`.`code`, `project`.`type`, `project`.`label`, `project`.`url` FROM `kpi`, `project` WHERE `kpi`.`project` = `project`.`id` AND `kpi`.`dump` = \''.$dump.'\' AND `kpi`.`birthyear` = 0 AND `kpi`.`country` = 0 AND `kpi`.`occupation` = 0 ORDER BY `project`.`label` DESC, `project`.`code` DESC');
            while ($project = $res->fetch_object()) {
                if (!isset($projects[$project->code])) {
                    $project->dump = $dump;
                    $projects[$project->code] = $project;
                }
            }
        }
        return array_reverse($projects);
    }
    
    public static function buildLink($options = array()) {
        $defaults = array(
            'kpi' => @GAPS_KPI_LINK,
            'project' => @GAPS_PROJECT_CODE,
            'country' => @GAPS_COUNTRY,
            'occupation' => @GAPS_OCCUPATION,
            'year' => @GAPS_YEAR_LINK,
            'year_start' => @GAPS_YEAR_START_LINK,
            'year_end' => @GAPS_YEAR_END_LINK,
            'threshold' => @GAPS_PEOPLE_THRESHOLD_LINK,
            'dump' => @GAPS_DUMPDATE_LINK,
            'sort' => @GAPS_SORT_LINK
        );
        $values = array();
        foreach ($defaults as $key => $value) {
            if (isset(self::$usedFilters[$key]) && self::$usedFilters[$key]) {
                if (isset($options[$key])) {
                    if (!empty($options[$key])) {
                        $values[] = $key.'='.$options[$key];
                    }
                } elseif (!empty($defaults[$key])) {
                    $values[] = $key.'='.$defaults[$key];
                }
            }
        }
        if (!empty($options['anchor'])) {
            $anchor = '#'.$options['anchor'];
        } else {
            $anchor = '';
        }
        return SITE_DIR.self::$page.(count($values) >= 1 ? '?'.implode('&amp;', $values) : '').$anchor;
    }
    
}

?>