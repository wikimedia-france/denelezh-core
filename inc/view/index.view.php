<?php

require '../inc/view/header.view.php';

$data = page::get('enwiki_latest_data');

?>

<p><strong><?php echo SITE_TITLE; ?></strong> provides statistics about the <a href="<?php echo SITE_DIR; ?>gender-gap/">gender gap</a> in the content of Wikimedia projects.</p>
<p>For example, <strong>as of <?php echo date('F j, Y', strtotime(page::get('latest_dump'))); ?>, only <?php echo number_format(100 / ($data->females + $data->males + $data->others) * $data->females, 1); ?> % of biographies in the English Wikipedia are about women</strong>.</p>
<p><?php echo SITE_TITLE; ?> allows you to explore the gender gap by several dimensions:</p>
<ul>
    <li><a href="<?php echo SITE_DIR; ?>gender-gap/#year">Gender Gap by year of birth</a></li>
    <li><a href="<?php echo SITE_DIR; ?>gender-gap/#country">Gender Gap by country of citizenship</a></li>
    <li><a href="<?php echo SITE_DIR; ?>gender-gap/#occupation">Gender Gap by occupation</a></li>
    <li><a href="<?php echo SITE_DIR; ?>gender-gap/#project">Gender Gap by Wikimedia project</a></li>
</ul>
<p>You can mix these four dimensions, for example to gather <a href="<?php echo SITE_DIR; ?>gender-gap/?project=dewiki&amp;country=142&amp;occupation=82955&amp;year=specified&amp;year_start=1801&amp;year_end=1900">data about the biographies</a> in the German Wikipedia about French politicians born in the 19th century.</p>
<p>Last but not least, <?php echo SITE_TITLE; ?> allows you to track the <a href="<?php echo SITE_DIR; ?>evolution/">evolution of gender gap</a> in Wikimedia projects since the beginning of 2017.</p>
<p>You can learn more about <?php echo SITE_TITLE; ?> and how it works by reading its <a href="<?php echo SITE_DIR; ?>documentation/">documentation</a>.</p>

<?php

require '../inc/view/footer.view.php';

?>