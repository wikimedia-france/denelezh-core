<?php

require '../inc/view/header.view.php';

?>

<h2>Filters</h2>
<form method="get" action="<?php echo SITE_DIR; ?>evolution/">
    <p>
        <?php
            echo display::form_project();
            echo display::form_country();
            echo display::form_occupation();
            echo display::form_year();
        ?>
        <label></label> <input type="submit" value="Apply filters" />
    </p>
</form>

<h2 class="clear">Humans in Wikidata</h2>
<div class="clear">
    <div class="evolution_graph">
        <p><canvas id="humans_raw"></canvas></p>
        <p class="legend">Number of humans and humans by gender.</p>
    </div>
    <div class="evolution_graph">
        <p><canvas id="humans_raw_der"></canvas></p>
        <p class="legend">Evolution (average per week).</p>
    </div>
</div>
<div class="clear">
    <div class="evolution_graph">
        <p><canvas id="humans_percent"></canvas></p>
        <p class="legend">Share by gender.</p>
    </div>
    <div class="evolution_graph">
        <p><canvas id="humans_percent_der"></canvas></p>
        <p class="legend">Evolution of share by gender (average per week).</p>
    </div>
</div>

<h2 class="clear">Humans in Wikidata with at least one sitelink</h2>
<?php if (defined('GAPS_PROJECT_ID') && !empty(GAPS_PROJECT_ID)): ?>
<p>As a project is selected, these graphs are identical to those in the previous section and are not displayed again ;-)</p>
<?php else: ?>
<div class="clear">
    <div class="evolution_graph">
        <p><canvas id="with_sitelink_raw"></canvas></p>
        <p class="legend">Number of humans and humans by gender.</p>
    </div>
    <div class="evolution_graph">
        <p><canvas id="with_sitelink_raw_der"></canvas></p>
        <p class="legend">Evolution (average per week).</p>
    </div>
</div>
<div class="clear">
    <div class="evolution_graph">
        <p><canvas id="with_sitelink_percent"></canvas></p>
        <p class="legend">Share by gender.</p>
    </div>
    <div class="evolution_graph">
        <p><canvas id="with_sitelink_percent_der"></canvas></p>
        <p class="legend">Evolution of share by gender (average per week).</p>
    </div>
</div>
<?php endif; ?>

<h2 class="clear">Number of sitelinks for humans in Wikidata</h2>
<div class="clear">
    <div class="evolution_graph">
        <p><canvas id="sitelinks_raw"></canvas></p>
        <p class="legend">Number of sitelinks for humans and humans by gender.</p>
    </div>
    <div class="evolution_graph">
        <p><canvas id="sitelinks_raw_der"></canvas></p>
        <p class="legend">Evolution (average per week).</p>
    </div>
</div>
<div class="clear">
    <div class="evolution_graph">
        <p><canvas id="sitelinks_percent"></canvas></p>
        <p class="legend">Share by gender.</p>
    </div>
    <div class="evolution_graph">
        <p><canvas id="sitelinks_percent_der"></canvas></p>
        <p class="legend">Evolution of share by gender (average per week).</p>
    </div>
</div>

<div class="clear"></div>

<script>
window.onload = function() {
    
    <?php $data = page::get('data'); ?>
    
    var dumps = [<?php echo '\''.implode('\', \'', $data['dumps']).'\''; ?>];
    var dumps_der = [<?php echo '\''.implode('\', \'', $data['dumps_der']).'\''; ?>];
    
    genderGapGraph('humans_raw', dumps, [<?php echo implode(', ', $data['humans']['all']); ?>], [<?php echo implode(', ', $data['females']['all']); ?>], [<?php echo implode(', ', $data['males']['all']); ?>], [<?php echo implode(', ', $data['others']['all']); ?>]);
    genderGapGraphDer('humans_raw_der', dumps_der, [<?php echo implode(', ', $data['humans_der']['all']); ?>], [<?php echo implode(', ', $data['females_der']['all']); ?>], [<?php echo implode(', ', $data['males_der']['all']); ?>], [<?php echo implode(', ', $data['others_der']['all']); ?>]);
    
    genderGapGraphPercent('humans_percent', dumps, [<?php echo implode(', ', $data['females_percent']['all']); ?>], [<?php echo implode(', ', $data['males_percent']['all']); ?>], [<?php echo implode(', ', $data['others_percent']['all']); ?>]);
    genderGapGraphDer('humans_percent_der', dumps_der, [], [<?php echo implode(', ', $data['females_percent_der']['all']); ?>], [<?php echo implode(', ', $data['males_percent_der']['all']); ?>], [<?php echo implode(', ', $data['others_percent_der']['all']); ?>]);
    
    <?php if (!defined('GAPS_PROJECT_ID') || empty(GAPS_PROJECT_ID)): ?>
    
    genderGapGraph('with_sitelink_raw', dumps, [<?php echo implode(', ', $data['humans']['with_sitelink']); ?>], [<?php echo implode(', ', $data['females']['with_sitelink']); ?>], [<?php echo implode(', ', $data['males']['with_sitelink']); ?>], [<?php echo implode(', ', $data['others']['with_sitelink']); ?>]);
    genderGapGraphDer('with_sitelink_raw_der', dumps_der, [<?php echo implode(', ', $data['humans_der']['with_sitelink']); ?>], [<?php echo implode(', ', $data['females_der']['with_sitelink']); ?>], [<?php echo implode(', ', $data['males_der']['with_sitelink']); ?>], [<?php echo implode(', ', $data['others_der']['with_sitelink']); ?>]);
    
    genderGapGraphPercent('with_sitelink_percent', dumps, [<?php echo implode(', ', $data['females_percent']['with_sitelink']); ?>], [<?php echo implode(', ', $data['males_percent']['with_sitelink']); ?>], [<?php echo implode(', ', $data['others_percent']['with_sitelink']); ?>]);
    genderGapGraphDer('with_sitelink_percent_der', dumps_der, [], [<?php echo implode(', ', $data['females_percent_der']['with_sitelink']); ?>], [<?php echo implode(', ', $data['males_percent_der']['with_sitelink']); ?>], [<?php echo implode(', ', $data['others_percent_der']['with_sitelink']); ?>]);
    
    <?php endif; ?>
    
    genderGapGraph('sitelinks_raw', dumps, [<?php echo implode(', ', $data['humans']['sitelinks']); ?>], [<?php echo implode(', ', $data['females']['sitelinks']); ?>], [<?php echo implode(', ', $data['males']['sitelinks']); ?>], [<?php echo implode(', ', $data['others']['sitelinks']); ?>]);
    genderGapGraphDer('sitelinks_raw_der', dumps_der, [<?php echo implode(', ', $data['humans_der']['sitelinks']); ?>], [<?php echo implode(', ', $data['females_der']['sitelinks']); ?>], [<?php echo implode(', ', $data['males_der']['sitelinks']); ?>], [<?php echo implode(', ', $data['others_der']['sitelinks']); ?>]);
    
    genderGapGraphPercent('sitelinks_percent', dumps, [<?php echo implode(', ', $data['females_percent']['sitelinks']); ?>], [<?php echo implode(', ', $data['males_percent']['sitelinks']); ?>], [<?php echo implode(', ', $data['others_percent']['sitelinks']); ?>]);
    genderGapGraphDer('sitelinks_percent_der', dumps_der, [], [<?php echo implode(', ', $data['females_percent_der']['sitelinks']); ?>], [<?php echo implode(', ', $data['males_percent_der']['sitelinks']); ?>], [<?php echo implode(', ', $data['others_percent_der']['sitelinks']); ?>]);
    
};
</script>

<?php

require '../inc/view/footer.view.php';

?>