<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?php $title = htmlentities(strip_tags(page::get('title'))); echo $title; if ($title !== SITE_TITLE): echo ' &mdash; '.SITE_TITLE; endif; ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo SITE_STATIC_DIR; ?>css/style.css" />
    <script type="text/javascript" src="<?php echo SITE_STATIC_DIR; ?>js/moment.min-2.24.0.js"></script>
    <script type="text/javascript" src="<?php echo SITE_STATIC_DIR; ?>js/chart.min.js"></script>
    <script type="text/javascript" src="<?php echo SITE_STATIC_DIR; ?>js/script.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="<?php echo $title; if ($title !== SITE_TITLE): echo ' &mdash; '.SITE_TITLE; endif; ?>" />
    <meta name="twitter:description" content="<?php echo SITE_TITLE; ?>: a tool to estimate the gender gap in Wikimedia projects." />
    <meta name="twitter:image" content="<?php echo SITE_STATIC_DIR; ?>img/twitter.png" />
    <meta property="og:title" content="<?php echo $title; if ($title !== SITE_TITLE): echo ' &mdash; '.SITE_TITLE; endif; ?>" />
    <meta property="og:description" content="<?php echo SITE_TITLE; ?>: a tool to estimate the gender gap in Wikimedia projects." />
    <meta property="og:image" content="<?php echo SITE_STATIC_DIR; ?>img/twitter.png" />
    <?php if (page::get('index') === false): echo '<meta name="robots" content="noindex, follow" />'."\n"; endif; ?>
</head>
<body>
<div id="header">
    <h1><?php echo '<a href="'.SITE_DIR.'"><img src="'.SITE_STATIC_DIR.'img/logo-denelezh.png" alt="" /> '.htmlentities(SITE_TITLE).'</a>'; if ($title != SITE_TITLE): echo ' &mdash; '.page::get('title'); endif; ?></h1>
</div>
<div id="menu">
    <ul>
        <li><a href="<?php echo SITE_DIR; ?>gender-gap/">Gender Gap</a></li>
        <li><a href="<?php echo SITE_DIR; ?>evolution/">Evolution</a></li>
        <li><a href="<?php echo SITE_DIR; ?>documentation/">Documentation</a></li>
    </ul>
</div>
<div id="content">

<?php
if (page::get('message') !== null) {
	echo '<div id="message">'.htmlentities(page::get('message')).'</div>';
}
?>