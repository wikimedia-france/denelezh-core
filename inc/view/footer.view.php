</div>
<div id="footer"><p><a href="https://creativecommons.org/licenses/by/4.0/">CC BY 4.0</a> / 2017-<?php echo date('Y'); ?> / <a href="<?php echo SITE_DIR; ?>"><?php echo SITE_TITLE; ?></a> <a href="https://twitter.com/denelezh"><img src="<?php echo SITE_STATIC_DIR; ?>img/logo-twitter.svg" alt="Denelezh on Twitter" title="<?php echo htmlentities(SITE_TITLE); ?> on Twitter" /></a> <a href="https://www.lehir.net/">Envel Le Hir</a> &amp; <a href="https://www.wikimedia.fr/">Wikimédia France</a></p></div>
</body>
</html>