<?php

require '../inc/view/header.view.php';

echo '<p>This page lists Wikimedia projects, sorted by the date of the first Wikidata dump in which they appeared (and that have been loaded into '.SITE_TITLE.').</p>';

$current_dump = null;
foreach (page::get('projects') as $project) {
    if ($project->dump !== $current_dump) {
        if ($current_dump !== null) {
            echo '</ul>';
        }
        $current_dump = $project->dump;
        echo '<h2>'.$current_dump.'</h2><ul>';
    }
    echo '<li><a href="'.SITE_DIR.'gender-gap/?project='.$project->code.'" title="Gender gap in '.htmlentities($project->label).'"><img src="'.SITE_STATIC_DIR.'img/chart_bar.png" alt="" class="logo" /></a> <a href="'.SITE_DIR.'evolution/?project='.$project->code.'" title="Evolution of gender gap in '.htmlentities($project->label).'"><img src="'.SITE_STATIC_DIR.'img/chart_curve.png" alt="" class="logo" /></a> <a href="'.htmlentities($project->url).'" title="'.$project->code.'"><img src="'.SITE_STATIC_DIR.'img/logo-'.$project->type.'.png" alt="" class="logo" /></a> '.htmlentities($project->label).' ('.$project->code.')</li>';
}
echo '</ul>';

require '../inc/view/footer.view.php';

?>