<?php

require '../inc/view/header.view.php';

?>

<h2>Filters</h2>
<form method="get" action="<?php echo SITE_DIR; ?>gender-gap/">
    <p>
        <label for="kpi">Count</label> <select id="kpi" name="kpi"><option value="humans"<?php if (GAPS_KPI == 'humans') { echo ' selected="selected"'; } ?>>Humans in Wikidata</option><option value="with_sitelink"<?php if (GAPS_KPI == 'with_sitelink') { echo ' selected="selected"'; } ?>>Humans in Wikidata with at least one sitelink</option><option value="sitelinks"<?php if (GAPS_KPI == 'sitelinks') { echo ' selected="selected"'; } ?>>Number of sitelinks for humans in Wikidata</option></select><br />
        <?php
            echo display::form_project();
            echo display::form_country();
            echo display::form_occupation();
            echo display::form_year();
        ?>
        <label for="form_threshold">Subset threshold</label> <input id="form_threshold" name="threshold" type="text" value="<?php echo GAPS_PEOPLE_THRESHOLD; ?>" /><?php if (GAPS_PEOPLE_THRESHOLD != 'auto'): echo ' <a href="'.gaps::buildLink(array('threshold' => '')).'" title="set to auto value"><img src="'.SITE_STATIC_DIR.'img/cross.png" alt="" /></a>'; endif; ?><br />
        <label for="form_dump">Wikidata dump</label> <select id="form_dump" name="dump"><option value="latest">latest (<?php echo gaps::dumps()[0]; ?>)</option><?php foreach (gaps::dumps() as $dump) { echo '<option value="'.$dump.'"'; if ($dump == GAPS_DUMPDATE_LABEL) { echo ' selected="selected"'; } echo '>'.$dump.'</option>'; } ?></select><?php if (GAPS_DUMPDATE_LABEL != 'latest'): echo ' <a href="'.gaps::buildLink(array('dump' => '')).'" title="set to latest available"><img src="'.SITE_STATIC_DIR.'img/cross.png" alt="" /></a>'; endif; ?><br />
        <label></label> <input type="hidden" name="sort" value="<?php echo GAPS_SORT; ?>" /><input type="submit" value="Apply filters" />
    </p>
</form>

<!-- global -->
<h2 id="global">Global Gender Gap</h2>
<table><?php
    $data = page::get('data');
    display::tableHeader('');
    display::tableRow('', $data['global']->humans, $data['global']->females, $data['global']->males, $data['global']->others);
?></table>

<!-- project -->
<?php if (isset($data['project'])): ?>
    <h2>[<?php echo htmlentities(GAPS_PROJECT_LABEL); ?>] Gender Gap</h2>
    <table><?php
        display::tableHeader('');
        display::tableRow('', $data['project']->humans, $data['project']->females, $data['project']->males, $data['project']->others);
    ?></table>
<?php endif; ?>

<!-- year -->
<?php if (isset($data['year'])): ?>
    <h2><?php if (!empty(GAPS_PROJECT_LABEL)): echo '['.htmlentities(GAPS_PROJECT_LABEL).'] '; endif; ?>Gender Gap from <?php echo htmlentities(GAPS_YEAR_LABEL); ?></h2>
    <table><?php
        display::tableHeader('Years of birth');
        display::tableRow(htmlentities(GAPS_YEAR_LABEL), $data['year']->humans, $data['year']->females, $data['year']->males, $data['year']->others);
    ?></table>
<?php endif; ?>

<!-- country -->
<?php if (isset($data['country'])): ?>
    <h2><?php if (!empty(GAPS_PROJECT_LABEL)): echo '['.htmlentities(GAPS_PROJECT_LABEL).'] '; endif; ?>Gender Gap in <?php echo htmlentities(GAPS_COUNTRY_LABEL); ?></h2>
    <table><?php
        display::tableHeader('Country');
        display::tableRow('<a href="https://www.wikidata.org/wiki/Q'.GAPS_COUNTRY.'" title="Q'.GAPS_COUNTRY.'"><img src="'.SITE_STATIC_DIR.'img/logo-wikidata.png" alt="" class="logo" /></a> '.htmlentities(GAPS_COUNTRY_LABEL), $data['country']->humans, $data['country']->females, $data['country']->males, $data['country']->others);
    ?></table>
<?php endif; ?>

<!-- occupation -->
<?php if (isset($data['occupation'])): ?>
    <h2><?php if (!empty(GAPS_PROJECT_LABEL)): echo '['.htmlentities(GAPS_PROJECT_LABEL).'] '; endif; ?>Gender Gap for <?php echo htmlentities(GAPS_OCCUPATION_LABEL); ?></h2>
    <table><?php
        display::tableHeader('Occupation');
        display::tableRow('<a href="https://www.wikidata.org/wiki/Q'.GAPS_OCCUPATION.'" title="Q'.GAPS_OCCUPATION.'"><img src="'.SITE_STATIC_DIR.'img/logo-wikidata.png" alt="" class="logo" /></a> '.htmlentities(GAPS_OCCUPATION_LABEL), $data['occupation']->humans, $data['occupation']->females, $data['occupation']->males, $data['occupation']->others);
    ?></table>
<?php endif; ?>

<!-- year + country -->
<?php if (isset($data['year_country'])): ?>
    <h2><?php if (!empty(GAPS_PROJECT_LABEL)): echo '['.htmlentities(GAPS_PROJECT_LABEL).'] '; endif; ?>Gender Gap in <?php echo htmlentities(GAPS_COUNTRY_LABEL); ?> from <?php echo htmlentities(GAPS_YEAR_LABEL); ?></h2>
    <table><?php
        display::tableHeader('');
        display::tableRow('', $data['year_country']->humans, $data['year_country']->females, $data['year_country']->males, $data['year_country']->others);
    ?></table>
<?php endif; ?>

<!-- year + occupation -->
<?php if (isset($data['year_occupation'])): ?>
    <h2><?php if (!empty(GAPS_PROJECT_LABEL)): echo '['.htmlentities(GAPS_PROJECT_LABEL).'] '; endif; ?>Gender Gap for <?php echo htmlentities(GAPS_OCCUPATION_LABEL); ?> from <?php echo htmlentities(GAPS_YEAR_LABEL); ?></h2>
    <table><?php
        display::tableHeader('');
        display::tableRow('', $data['year_occupation']->humans, $data['year_occupation']->females, $data['year_occupation']->males, $data['year_occupation']->others);
    ?></table>
<?php endif; ?>

<!-- country + occupation -->
<?php if (isset($data['country_occupation'])): ?>
    <h2><?php if (!empty(GAPS_PROJECT_LABEL)): echo '['.htmlentities(GAPS_PROJECT_LABEL).'] '; endif; ?>Gender Gap for <?php echo htmlentities(GAPS_OCCUPATION_LABEL); ?> in <?php echo htmlentities(GAPS_COUNTRY_LABEL); ?></h2>
    <table><?php
        display::tableHeader('');
        display::tableRow('', $data['country_occupation']->humans, $data['country_occupation']->females, $data['country_occupation']->males, $data['country_occupation']->others);
    ?></table>
<?php endif; ?>

<!-- year + country + occupation -->
<?php if (isset($data['year_country_occupation'])): ?>
    <h2><?php if (!empty(GAPS_PROJECT_LABEL)): echo '['.htmlentities(GAPS_PROJECT_LABEL).'] '; endif; ?>Gender Gap for <?php echo htmlentities(GAPS_OCCUPATION_LABEL); ?> in <?php echo htmlentities(GAPS_COUNTRY_LABEL); ?> from <?php echo htmlentities(GAPS_YEAR_LABEL); ?></h2>
    <table><?php
        display::tableHeader('');
        display::tableRow('', $data['year_country_occupation']->humans, $data['year_country_occupation']->females, $data['year_country_occupation']->males, $data['year_country_occupation']->others);
    ?></table>
<?php endif; ?>

<!-- no year -->
<?php if (isset($data['no_year'])): ?>
    <h2 id="year"><?php if (!empty(GAPS_PROJECT_LABEL)): echo '['.htmlentities(GAPS_PROJECT_LABEL).'] '; endif; ?>Gender Gap by year of birth<?php if (!empty(GAPS_OCCUPATION)) { echo ' for '.htmlentities(GAPS_OCCUPATION_LABEL); } if (!empty(GAPS_COUNTRY)) { echo ' in '.htmlentities(GAPS_COUNTRY_LABEL); } ?></h2>
    <table><?php
        display::tableHeader('Years of birth');
        foreach ($data['no_year'] as $year => $row) {
            display::tableRow('<a href="'.gaps::buildLink(array('year' => 'specified', 'year_start' => $year, 'year_end' => ($year + 9))).'">'.$year.' → '.($year + 9).'</a>', $row->humans, $row->females, $row->males, $row->others);
        }
    ?></table>
<?php endif; ?>

<!-- no country -->
<?php if (isset($data['no_country'])): ?>
    <h2 id="country"><?php if (!empty(GAPS_PROJECT_LABEL)): echo '['.htmlentities(GAPS_PROJECT_LABEL).'] '; endif; ?>Gender Gap by country of citizenship<?php if (!empty(GAPS_OCCUPATION)) { echo ' for '.htmlentities(GAPS_OCCUPATION_LABEL); } if (!empty(GAPS_YEAR_LABEL)) { echo ' from '.htmlentities(GAPS_YEAR_LABEL); } ?></h2>
    <table><?php
        display::tableHeader('Country', true, 'country');
        if (count($data['no_country']) === 0) {
            display::tableEmptyTableRow('no country above the threshold of '.GAPS_PEOPLE_THRESHOLD_VALUE.' people');
        }
        foreach ($data['no_country'] as $row) {
            display::tableRow('<a href="https://www.wikidata.org/wiki/Q'.$row->id.'" title="Q'.$row->id.'"><img src="'.SITE_STATIC_DIR.'img/logo-wikidata.png" alt="" class="logo" /></a> <a href="'.gaps::buildLink(array('country' => $row->id)).'">'.display::buildLabel($row->id, $row->label).'</a>', $row->humans, $row->females, $row->males, $row->others);
        }
    ?></table>
<?php endif; ?>

<!-- no occupation -->
<?php if (isset($data['no_occupation'])): ?>
    <h2 id="occupation"><?php if (!empty(GAPS_PROJECT_LABEL)): echo '['.htmlentities(GAPS_PROJECT_LABEL).'] '; endif; ?>Gender Gap by occupation<?php if (!empty(GAPS_COUNTRY)) { echo ' in '.htmlentities(GAPS_COUNTRY_LABEL); } if (!empty(GAPS_YEAR_LABEL)) { echo ' from '.htmlentities(GAPS_YEAR_LABEL); } ?></h2>
    <table><?php
        display::tableHeader('Occupation', true, 'occupation');
        if (count($data['no_occupation']) === 0) {
            display::tableEmptyTableRow('no occupation above the threshold of '.GAPS_PEOPLE_THRESHOLD_VALUE.' people');
        }
        foreach ($data['no_occupation'] as $row) {
            display::tableRow('<a href="https://www.wikidata.org/wiki/Q'.$row->id.'" title="Q'.$row->id.'"><img src="'.SITE_STATIC_DIR.'img/logo-wikidata.png" alt="" class="logo" /></a> <a href="'.gaps::buildLink(array('occupation' => $row->id)).'">'.display::buildLabel($row->id, $row->label).'</a>', $row->humans, $row->females, $row->males, $row->others);
        }
    ?></table>
<?php endif; ?>

<!-- no project -->
<?php if (isset($data['no_project'])): ?>
    <h2 id="project">Gender Gap by Wikimedia project<?php if (!empty(GAPS_OCCUPATION)) { echo ' for '.htmlentities(GAPS_OCCUPATION_LABEL); } if (!empty(GAPS_COUNTRY)) { echo ' in '.htmlentities(GAPS_COUNTRY_LABEL); } if (!empty(GAPS_YEAR_LABEL)) { echo ' from '.htmlentities(GAPS_YEAR_LABEL); } ?></h2>
    <table><?php
        display::tableHeader('Project', true, 'project');
        if (count($data['no_project']) === 0) {
            display::tableEmptyTableRow('no project above the threshold of '.GAPS_PEOPLE_THRESHOLD_VALUE.' people');
        }
        foreach ($data['no_project'] as $row) {
            display::tableRow((!empty($row->type) ? '<a href="'.htmlentities($row->url).'" title="'.$row->code.'"><img src="'.SITE_STATIC_DIR.'img/logo-'.$row->type.'.png" alt="" class="logo" /></a> ' : '').'<a href="'.gaps::buildLink(array('project' => $row->code)).'">'.htmlentities($row->label).'</a>', $row->humans, $row->females, $row->males, $row->others);
        }
    ?></table>
<?php endif; ?>

<?php

require '../inc/view/footer.view.php';

?>