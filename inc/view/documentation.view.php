<?php

require '../inc/view/header.view.php';

$entries = array();

function addEntry(&$entries, $anchor, $question, $solution) {
    $entries[] = array('anchor' => $anchor, 'question' => $question, 'solution' => $solution);
}

addEntry($entries, 'statistics', 'How are statistics generated in '.htmlentities(SITE_TITLE).'?', '<p>'.htmlentities(SITE_TITLE).' uses <a href="https://www.wikidata.org/">Wikidata</a>, the centralized knowledge base of Wikimedia projects, to generate statistics.</p>
<p>With each weekly dump of Wikidata, statistics are produced following these rules:</p>
<ul class="stickToPrevious">
    <li>A Wikidata item with a best value (not necessarily unique) for the property <em>instance of</em> equal to <em>human</em> is a human.</li>
    <li>A human with its (unique) best value for the property <em>gender</em> equal to <em>female</em> is a female, equal to <em>male</em> is a male, equal to any other value is an other. A human with zero or multiple values as best value for the property <em>gender</em> has no gender in '.htmlentities(SITE_TITLE).'.</li>
    <li>A human has a year of birth if the property <em>date of birth</em> has a best value with the sufficient precision. In the case of several best values available, the year is used if it is equal for each value, otherwise the human has no year of birth.</li>
    <li>All normal and preferred values of the property <em>country of citizenship</em> are used as country of citizenship for each human.</li>
    <li>All normal and preferred values of the property <em>occupation</em> are used as occupation for each human.</li>
    <li>Parent occupations are deduced using the property <em>subclass of</em>. An occupation has to be directly used at least one time to appears in '.htmlentities(SITE_TITLE).'.</li>
    <li>Countries used less than 200 times are discarded.</li>
    <li>Occupations used (directly or indirectly) less than 1,000 times are discarded.</li>
    <li>A sitelink represents a page in a Wikimedia project (including all Wikipedias, but also Wikisource, Wikiquote, ...) for a given item.</li>
</ul>

<h3>What are the ranks in Wikidata?</h3>
<p>To sum up, each statement in Wikidata has a rank:</p>
<ul class="stickToPrevious stickToNext">
    <li><strong>deprecated</strong>: the value is incorrect;</li>
    <li><strong>normal</strong> (the default rank): the value is correct;</li>
    <li><strong>preferred</strong>: the value is the best among the correct values.</li>
</ul>
<p>The <strong>best</strong> rank represent the best values that are available for a property in an item: the ones with the preferred rank if they exist, the ones with the normal rank otherwise.</p>');

addEntry($entries, 'wikipedia', 'How can I get the number of biographies accross all Wikipedias?', '<p>At the moment, '.htmlentities(SITE_TITLE).' only provides statistics about a specific Wikimedia project or all Wikimedia projects together, not a subset of them.</p>');

addEntry($entries, 'history', 'What is the history of '.htmlentities(SITE_TITLE).'?', '<p>The first version of Denelezh was released in March 2017. The second version, aka Denelezh 2.0, was released in April 2018, including many improvements:</p>
<ul class="stickToPrevious">
    <li>The gender gap by Wikimedia project is available.</li>
    <li>Statistics are made with all humans in Wikidata (and not only the ones with gender + year of birth + country of citizenship + occupation).</li>
    <li>There is no limit to the year of birth (statistics about humans born before 1600 are available).</li>
    <li>Occupations are deduced using the property « subclass of ».</li>
</ul>
<p>Each major version was detailed in a blog post:</p>
<ul class="stickToPrevious">
    <li>First version, March 2017: <a href="https://www.lehir.net/a-tool-to-estimate-the-gender-gap-in-wikidata-and-wikipedia/">A tool to estimate the gender gap in Wikidata and Wikipedia</a>.</li>
    <li>Second version, April 2018: <a href="https://www.lehir.net/denelezh-2-0-a-transitional-version/">Denelezh 2.0, a transitional version</a>.</li>
</ul>
<p>Starting in March 2019, '.htmlentities(SITE_TITLE).' is hosted by <a href="https://www.wikimedia.fr/">Wikimédia France</a>.</p>');

addEntry($entries, 'name', 'What is the origin of the name <em>'.htmlentities(SITE_TITLE).'</em>?', '<p><em>'.htmlentities(SITE_TITLE).'</em> means <em>Humanity</em> (the sum of all humans) in Breton.</p>');

addEntry($entries, 'source', 'Where can I find the source code of '.htmlentities(SITE_TITLE).'?', '<p>'.htmlentities(SITE_TITLE).' is a free and open source project released under <a href="https://www.gnu.org/licenses/agpl-3.0.en.html">AGPLv3 license</a>. It is made up of two sub-projects:</p>
<ul class="stickToPrevious">
    <li><a href="https://framagit.org/wikimedia-france/denelezh-core">denelezh-core</a>;</li>
    <li><a href="https://framagit.org/wikimedia-france/denelezh-import">denelezh-import</a>.</li>
</ul>');

addEntry($entries, 'tools', 'Do similar tools exist?', '<p>Several tools providing statistics about the <em>content</em> of Wikimedia projects exist (sorted here by year of release):</p>
<ul class="stickToPrevious">
    <li>2014 &mdash; <a href="http://whgi.wmflabs.org/">Wikidata Human Gender Indicators</a> (WHGI), state of the art from an academic point of view; tracks the number of biographies in Wikidata:
        <ul>
            <li>gender gap by culture</li>
            <li>gender gap by country of birth</li>
            <li>gender gap by date of birth and by date of death</li>
            <li>gender gap by Wikipedia language</li>
        </ul>
    </li>
    <li>2017 &mdash; <a href="'.SITE_DIR.'">'.htmlentities(SITE_TITLE).'</a>, provides statistics about Wikidata items depicting human beings (all, with at least one sitelink, and the number of sitelinks):
        <ul>
            <li>gender gap by Wikimedia project</li>
            <li>gender gap by country of citizenship</li>
            <li>gender gap by occupation</li>
            <li>gender gap by date of birth</li>
            <li>multidimensional analysis (the ability to combine previous axis)</li>
            <li>evolution of gender gap</li>
        </ul>
    </li>
    <li>2018 &mdash; <a href="http://wdcm.wmflabs.org/WDCM_BiasesDashboard/">WDCM Biases Dashboard</a>, tracks the usage of Wikidata items (in how many pages each item is used in Wikimedia projects, not the number of sitelinks):
        <ul>
            <li>gender gap by Wikimedia project</li>
            <li>gender gap by occupation</li>
            <li>gender gap by North-South divide</li>
        </ul>
    </li>
</ul>');

addEntry($entries, 'logos', 'Who are the authors and what are the licenses of the images and logos used on '.htmlentities(SITE_TITLE).'?', '<p>Images and logos used on '.htmlentities(SITE_TITLE).' have various authors and licenses. The full list is available on the <a href="https://framagit.org/wikimedia-france/denelezh-core/blob/master/NOTICE">NOTICE file</a> on the git repository of the project.</p>');

echo '<ol>';
foreach ($entries as $entry) {
    echo '<li><a href="'.SITE_DIR.'documentation/#'.$entry['anchor'].'">'.$entry['question'].'</a></li>';
}
echo '</ol>';

foreach ($entries as $entry) {
    echo '<h2 id="'.$entry['anchor'].'">'.$entry['question'].'</h2>'.$entry['solution'];
}

require '../inc/view/footer.view.php';

?>