<?php

require '../inc/load.inc.php';

try {
	
	// URL, args
	page::set('shortUrl', substr($_SERVER['REQUEST_URI'], strlen(SITE_SHORT_DIR)));
	preg_match_all('/\/([a-z0-9.-]+)/', page::get('shortUrl'), $args);
    if (isset($args[1][0])) {
        $args[1][0] = preg_replace('/\.php$/', '', $args[1][0]);
        if ($args[1][0] == 'doc') {
            $args[1][0] = 'documentation';
        }
    }
	page::set('args', $args[1]);
	
	// index
	if (count(page::get('args')) == 0) {
		require '../inc/controller/index.inc.php';
	}
	// other pages
	elseif (file_exists('../inc/controller/'.page::get('args')[0].'.inc.php')) {
		require '../inc/controller/'.page::get('args')[0].'.inc.php';
	}
	// unknown page
	else {
		throw new Exception($_SERVER['REQUEST_URI'], 404);
	}
	
	// existing view
	if (!file_exists('../inc/view/'.page::get('view').'.view.php')) {
		throw new Exception('Unknown view.', 503);
	}
	
// errors
} catch (Exception $e) {
	page::set('view', 'default');
	switch ($e->getCode()) {
		case 404:
			header('HTTP/1.0 404 Not Found');
			page::set('title', 'Page not found');
			page::set('message', 'This page does not exist.');
            error_log('404, '.$e->getMessage());
		break;
		default:
			header('HTTP/1.0 503 Service Unavailable');
			page::set('title', 'Service unavailable');
			page::set('message', 'The website is broken. Please try again later.');
            error_log('503, '.$e->getMessage());
	}
}

db::close();

// display
require '../inc/view/'.page::get('view').'.view.php';

?>