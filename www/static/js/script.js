function genderGapGraph(id, dumps, humans, females, males, others) {
    var config = {
        type: 'line',
        data: {
            labels: dumps,
            datasets: [{
                label: 'Humans',
                backgroundColor: 'black',
                borderColor: 'black',
                fill: false,
                data: humans,
                cubicInterpolationMode: 'monotone',
            },{
                label: 'Females',
                backgroundColor: 'orange',
                borderColor: 'orange',
                fill: false,
                data: females,
                cubicInterpolationMode: 'monotone',
            },{
                label: 'Males',
                backgroundColor: 'green',
                borderColor: 'green',
                fill: false,
                data: males,
                cubicInterpolationMode: 'monotone',
            },{
                label: 'Other genders',
                backgroundColor: 'blue',
                borderColor: 'blue',
                fill: false,
                data: others,
                cubicInterpolationMode: 'monotone',
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        unit: 'month',
                        displayFormats: { month: 'YYYY-MM' },
                    },
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        userCallback: function(value, index, values) { return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','); }
                    }
                }]
            },
            tooltips: { enabled: true, mode: 'single', callbacks: { label: function(tooltipItems, data) { return data.datasets[tooltipItems.datasetIndex].label + ': ' + tooltipItems.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','); } } }
        }
    };
    window.myLine = new Chart(document.getElementById(id).getContext('2d'), config);
}

function genderGapGraphPercent(id, dumps, females, males, others) {
    var config = {
        type: 'line',
        data: {
            labels: dumps,
            datasets: [{
                label: '% of females',
                backgroundColor: 'orange',
                borderColor: 'orange',
                fill: false,
                data: females,
                cubicInterpolationMode: 'monotone',
            },{
                label: '% of males',
                backgroundColor: 'green',
                borderColor: 'green',
                fill: false,
                data: males,
                cubicInterpolationMode: 'monotone',
            },{
                label: '% of other genders',
                backgroundColor: 'blue',
                borderColor: 'blue',
                fill: false,
                data: others,
                cubicInterpolationMode: 'monotone',
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        unit: 'month',
                        displayFormats: { month: 'YYYY-MM' },
                    },
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        max: 100,
                    }
                }]
            },
        }
    };
    window.myLine = new Chart(document.getElementById(id).getContext('2d'), config);
}

function genderGapGraphDer(id, dumps, humans, females, males, others) {
    var config = {
        type: 'line',
        data: {
            labels: dumps,
            datasets: [{
                label: 'Humans',
                backgroundColor: 'black',
                borderColor: 'black',
                fill: false,
                data: humans,
                cubicInterpolationMode: 'monotone',
            },{
                label: 'Females',
                backgroundColor: 'orange',
                borderColor: 'orange',
                fill: false,
                data: females,
                cubicInterpolationMode: 'monotone',
            },{
                label: 'Males',
                backgroundColor: 'green',
                borderColor: 'green',
                fill: false,
                data: males,
                cubicInterpolationMode: 'monotone',
            },{
                label: 'Other genders',
                backgroundColor: 'blue',
                borderColor: 'blue',
                fill: false,
                data: others,
                cubicInterpolationMode: 'monotone',
            }]
        },
        options: {
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                        unit: 'month',
                        displayFormats: { month: 'YYYY-MM' },
                    },
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        userCallback: function(value, index, values) { return (value > 0 ? '+' : '') + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','); }
                    }
                }]
            },
            tooltips: { enabled: true, mode: 'single', callbacks: { label: function(tooltipItems, data) { return data.datasets[tooltipItems.datasetIndex].label + ': ' + (tooltipItems.yLabel > 0 ? '+' : '') + tooltipItems.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ','); } } }
        }
    };
    window.myLine = new Chart(document.getElementById(id).getContext('2d'), config);
}
