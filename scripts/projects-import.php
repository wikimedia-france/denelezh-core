<?php

#
# Import metadata about Wikimedia projects.
#

require '../inc/load.inc.php';

function guessProject($project) {
    switch ($project->code->value) {
        case 'commonswiki':
            return 'commons';
        break;
        case 'mediawikiwiki':
            return 'mediawiki';
        break;
        case 'metawiki':
            return 'meta';
        break;
        case 'specieswiki':
            return 'wikispecies';
        break;
        case 'wikidatawiki':
            return 'wikidata';
        break;
    }
    if (strpos($project->instanceOf->value, 'http://www.wikidata.org/entity/Q22001316') !== false) {
        return 'wikibooks';
    }
    if (strpos($project->instanceOf->value, 'http://www.wikidata.org/entity/Q20671729') !== false) {
        return 'wikinews';
    }
    if (strpos($project->instanceOf->value, 'http://www.wikidata.org/entity/Q10876391') !== false) {
        return 'wikipedia';
    }
    if (strpos($project->instanceOf->value, 'http://www.wikidata.org/entity/Q22001361') !== false) {
        return 'wikiquote';
    }
    if (strpos($project->instanceOf->value, 'http://www.wikidata.org/entity/Q15156455') !== false) {
        return 'wikisource';
    }
    if (strpos($project->instanceOf->value, 'http://www.wikidata.org/entity/Q22001390') !== false) {
        return 'wikiversity';
    }
    if (strpos($project->instanceOf->value, 'http://www.wikidata.org/entity/Q19826567') !== false) {
        return 'wikivoyage';
    }
    if (strpos($project->instanceOf->value, 'http://www.wikidata.org/entity/Q22001389') !== false) {
        return 'wiktionary';
    }
    return null;
}

$res = file_get_contents('https://query.wikidata.org/sparql?format=json&query='.urlencode('SELECT ?item ?label ?code (GROUP_CONCAT(DISTINCT ?instanceOf) AS ?instanceOf) (GROUP_CONCAT(DISTINCT ?url) AS ?url) {
  ?item wdt:P31 ?instanceOf ; wdt:P856 ?url ; wdt:P1800 ?code ; rdfs:label ?label .
  FILTER(LANG(?label) = "en") .
}
GROUP BY ?item ?label ?code'), false, stream_context_create(array('http' => array('header' => 'User-Agent: Denelezh/2.0 (https://www.denelezh.org/; webmaster@denelezh.org)'))));
$data = json_decode($res)->results->bindings;

foreach ($data as $project) {
    $type = guessProject($project);
    db::query('UPDATE `project` SET `type` = '.($type != null ? '\''.db::sec($type).'\'' : 'NULL').', `label` = \''.db::sec($project->label->value).'\', `url` = \''.db::sec($project->url->value).'\' WHERE `code` = \''.db::sec($project->code->value).'\'');
}
db::commit();

?>