<?php

#
# This script removes data from a Wikidata dump in Denelezh database.
# Parameter: date of the dump (YYYY-MM-DD).
#

require '../inc/load.inc.php';

if ($argc !== 2) {
    throw new Exception('Invalid number of parameters: script.php <date of the dump>');
}

if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $argv[1])) {
    throw new Exception('Invalid parameter format: YYYY-MM-DD');
}

$date = $argv[1];

db::query('DELETE FROM `dump` WHERE `date` = \''.$date.'\'');
db::commit();
try {
    db::query('ALTER TABLE `kpi` DROP PARTITION `dump'.str_replace('-', '', $date).'`');
} catch (Exception $e) {
    echo 'Warning: exception while dropping table partition.'."\n";
}

echo 'Done.'."\n";

?>