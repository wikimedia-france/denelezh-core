<?php

#
# This script copies data from the table `kpi` to the table `kpi_2`, partition by partition. It is useful in case of schema change.
#

require '../inc/load.inc.php';

$res = db::query('SELECT `date` FROM `dump` WHERE `step` = 1');
while ($row = $res->fetch_object()) {
    echo $row->date.'...'."\n";
    db::query('INSERT INTO `kpi_2` SELECT * FROM `kpi` WHERE `dump` = \''.$row->date.'\'');
    db::commit();
    db::query('ALTER TABLE `kpi_2` REBUILD PARTITION `dump'.str_replace('-', '', $row->date).'`');
    echo 'Done.'."\n";
}

?>