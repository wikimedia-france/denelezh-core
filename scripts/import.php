<?php

require '../inc/load.inc.php';

d_log('Starting...');

// dump

$dump = file_get_contents('/tmp/dump.csv');
db::query('INSERT INTO `dump` VALUES(\''.$dump.'\', 0)');
$partition = 'dump'.str_replace('-', '', $dump);

// load compute tables

d_log('Loading `human` table...');
db::query('TRUNCATE TABLE `human`');
db::query('LOAD DATA INFILE \'/tmp/human.csv\' IGNORE INTO TABLE `human` FIELDS TERMINATED BY \',\'');

d_log('Loading `human_country` table...');
db::query('TRUNCATE TABLE `human_country`');
db::query('LOAD DATA INFILE \'/tmp/human_country.csv\' IGNORE INTO TABLE `human_country` FIELDS TERMINATED BY \',\'');

d_log('Loading `human_occupation` table...');
db::query('TRUNCATE TABLE `human_occupation`');
db::query('LOAD DATA INFILE \'/tmp/human_occupation.csv\' IGNORE INTO TABLE `human_occupation` FIELDS TERMINATED BY \',\'');

d_log('Loading `human_sitelink` table...');
db::query('TRUNCATE TABLE `human_sitelink`');
db::query('LOAD DATA INFILE \'/tmp/human_sitelink.csv\' IGNORE INTO TABLE `human_sitelink` FIELDS TERMINATED BY \',\'');

d_log('Loading `occupation_parent` table...');
db::query('TRUNCATE TABLE `occupation_parent`');
db::query('LOAD DATA INFILE \'/tmp/occupation_parent.csv\' IGNORE INTO TABLE `occupation_parent` FIELDS TERMINATED BY \',\'');

d_log('Loading `label` table...');
db::query('TRUNCATE TABLE `label`');
db::query('LOAD DATA INFILE \'/tmp/label.csv\' IGNORE INTO TABLE `label` FIELDS TERMINATED BY \',\' OPTIONALLY ENCLOSED BY \'"\' ESCAPED BY \'\\\\\'');

// load parent occupations

d_log('Adding parent occupations...');
db::query('INSERT IGNORE INTO `human_occupation`
SELECT `hc`.`human`, `op`.`parent`
FROM `human_occupation` `hc`, `occupation_parent` `op`
WHERE `hc`.`occupation` = `op`.`occupation`');

// remove countries and occupations barely used

d_log('Removing barely used countries...');
db::query('DELETE FROM human_country WHERE country IN (
	SELECT country FROM (
		SELECT country FROM human_country GROUP BY country HAVING COUNT(*) < '.GAPS_MINIMUM_THRESHOLD_COUNTRY.'
    ) a
)');

d_log('Removing barely used occupations...');
db::query('DELETE FROM human_occupation WHERE occupation IN (
	SELECT occupation FROM (
		SELECT occupation FROM human_occupation GROUP BY occupation HAVING COUNT(*) < '.GAPS_MINIMUM_THRESHOLD_OCCUPATION.'
    ) a
)');

// load reference tables

// load new years
// TODO remove years out of bounds
d_log('Loading `year` table...');
db::query('INSERT INTO `year`(`year`) SELECT DISTINCT `birthyear` FROM `human` WHERE `birthyear` IS NOT NULL AND `birthyear` NOT IN (SELECT `year` FROM `year`)');

// load new countries
d_log('Loading `country` table...');
db::query('INSERT INTO `country`(`id`, `label`, `date`) SELECT DISTINCT `human_country`.`country`, `label`.`label`, \''.$dump.'\' AS `date` FROM `human_country` LEFT JOIN `label` ON (`human_country`.`country` = `label`.`id`)
ON DUPLICATE KEY UPDATE `label` = (CASE WHEN `country`.`date` < VALUES(`date`) AND VALUES(`label`) IS NOT NULL THEN VALUES(`label`) ELSE `country`.`label` END), `date` = (CASE WHEN `country`.`date` < VALUES(`date`) AND VALUES(`label`) IS NOT NULL THEN VALUES(`date`) ELSE `country`.`date` END)');

// load new occupations
d_log('Loading `occupation` table...');
db::query('INSERT INTO `occupation`(`id`, `label`, `date`) SELECT DISTINCT `human_occupation`.`occupation`, `label`.`label`, \''.$dump.'\' AS `date` FROM `human_occupation` LEFT JOIN `label` ON (`human_occupation`.`occupation` = `label`.`id`)
ON DUPLICATE KEY UPDATE `label` = (CASE WHEN `occupation`.`date` < VALUES(`date`) AND VALUES(`label`) IS NOT NULL THEN VALUES(`label`) ELSE `occupation`.`label` END), `date` = (CASE WHEN `occupation`.`date` < VALUES(`date`) AND VALUES(`label`) IS NOT NULL THEN VALUES(`date`) ELSE `occupation`.`date` END)');

// load new projects
d_log('Loading `project` table...');
db::query('INSERT INTO `project`(`code`) SELECT DISTINCT `sitelink` FROM `human_sitelink` WHERE `sitelink` NOT IN (SELECT `code` FROM `project`)');

// kpi

d_log('Loading `kpi` table...');
db::query('ALTER TABLE `kpi` ADD PARTITION (PARTITION `'.$partition.'` VALUES IN (\''.$dump.'\'))');

$birthyear = array(
    'select' => array('0', '`year`.`id` AS `birthyear`'),
    'from' => array(null, '`year`'),
    'where' => array(null, '`human`.`birthyear` = `year`.`year`'),
    'groupby' => array(null, '`human`.`birthyear`'),
);
$country = array(
    'select' => array('0', '`human_country`.`country`'),
    'from' => array(null, '`human_country`'),
    'where' => array(null, '`human`.`id` = `human_country`.`human`'),
    'groupby' => array(null, '`human_country`.`country`'),
);
$occupation = array(
    'select' => array('0', '`human_occupation`.`occupation`'),
    'from' => array(null, '`human_occupation`'),
    'where' => array(null, '`human`.`id` = `human_occupation`.`human`'),
    'groupby' => array(null, '`human_occupation`.`occupation`'),
);
$project = array(
    'select' => array('0', '`project`.`id` AS `project`'),
    'from' => array(null, array('`human_sitelink`', '`project`')),
    'where' => array(null, array('`human`.`id` = `human_sitelink`.`human`', '`human_sitelink`.`sitelink` = `project`.`code`')),
    'groupby' => array(null, '`project`.`id`'),
);
for ($i_birthyear = 0; $i_birthyear < count($birthyear['select']); $i_birthyear++) {
    for ($i_country = 0; $i_country < count($country['select']); $i_country++) {
        for ($i_occupation = 0; $i_occupation < count($occupation['select']); $i_occupation++) {
            for ($i_project = 0; $i_project < count($project['select']); $i_project++) {
                // select
                $select = array('\''.$dump.'\' AS `dump`');
                addElementToArray($select, $birthyear['select'][$i_birthyear]);
                addElementToArray($select, $country['select'][$i_country]);
                addElementToArray($select, $occupation['select'][$i_occupation]);
                addElementToArray($select, $project['select'][$i_project]);
                $select[] = 'COUNT(*) AS `humans`';
                $select[] = 'SUM(CASE WHEN `human`.`gender` = 6581072 THEN 1 ELSE 0 END) AS `females`';
                $select[] = 'SUM(CASE WHEN `human`.`gender` = 6581097 THEN 1 ELSE 0 END) AS `males`';
                $select[] = 'SUM(CASE WHEN `human`.`gender` IS NOT NULL AND `human`.`gender` <> -1 AND `human`.`gender` <> 6581072 AND `human`.`gender` <> 6581097 THEN 1 ELSE 0 END) AS `others`';
                $select[] = 'SUM(CASE WHEN `human`.`sitelinks` >= 1 THEN 1 ELSE 0 END) AS `humans_with_sitelink`';
                $select[] = 'SUM(CASE WHEN `human`.`gender` = 6581072 AND `human`.`sitelinks` >= 1 THEN 1 ELSE 0 END) AS `females_with_sitelink`';
                $select[] = 'SUM(CASE WHEN `human`.`gender` = 6581097 AND `human`.`sitelinks` >= 1 THEN 1 ELSE 0 END) AS `males_with_sitelink`';
                $select[] = 'SUM(CASE WHEN `human`.`gender` IS NOT NULL AND `human`.`gender` <> -1 AND `human`.`gender` <> 6581072 AND `human`.`gender` <> 6581097 AND `human`.`sitelinks` >= 1 THEN 1 ELSE 0 END) AS `others_with_sitelink`';
                $select[] = 'SUM(`human`.`sitelinks`) AS `humans_sitelinks`';
                $select[] = 'SUM(CASE WHEN `human`.`gender` = 6581072 THEN `human`.`sitelinks` ELSE 0 END) AS `females_sitelinks`';
                $select[] = 'SUM(CASE WHEN `human`.`gender` = 6581097 THEN `human`.`sitelinks` ELSE 0 END) AS `males_sitelinks`';
                $select[] = 'SUM(CASE WHEN `human`.`gender` IS NOT NULL AND `human`.`gender` <> -1 AND `human`.`gender` <> 6581072 AND `human`.`gender` <> 6581097 THEN `human`.`sitelinks` ELSE 0 END) AS `others_sitelinks`';
                // from
                $from = array('`human`');
                addElementToArray($from, $birthyear['from'][$i_birthyear]);
                addElementToArray($from, $country['from'][$i_country]);
                addElementToArray($from, $occupation['from'][$i_occupation]);
                addElementToArray($from, $project['from'][$i_project]);
                // where
                $where = array();
                addElementToArray($where, $birthyear['where'][$i_birthyear]);
                addElementToArray($where, $country['where'][$i_country]);
                addElementToArray($where, $occupation['where'][$i_occupation]);
                addElementToArray($where, $project['where'][$i_project]);
                // groupby
                $groupby = array();
                addElementToArray($groupby, $birthyear['groupby'][$i_birthyear]);
                addElementToArray($groupby, $country['groupby'][$i_country]);
                addElementToArray($groupby, $occupation['groupby'][$i_occupation]);
                addElementToArray($groupby, $project['groupby'][$i_project]);
                // build query
                $query = 'INSERT INTO `kpi` SELECT '.implode(', ', $select).' FROM '.implode(', ', $from);
                if (!empty($where)) {
                    $query .= ' WHERE '.implode(' AND ', $where);
                }
                if (!empty($groupby)) {
                    $query .= ' GROUP BY '.implode(', ', $groupby);
                }
                // execute query
                d_log('Loading `kpi` table ['.$i_birthyear.$i_country.$i_occupation.$i_project.']...');
                db::query($query);
                db::commit();
            }
        }
    }
}

d_log('Optimizing tables...');
db::query('ALTER TABLE `kpi` REBUILD PARTITION `'.$partition.'`');
db::query('ANALYZE TABLE `kpi`');
db::query('OPTIMIZE TABLE `year`');
db::query('OPTIMIZE TABLE `country`');
db::query('OPTIMIZE TABLE `occupation`');
db::query('OPTIMIZE TABLE `project`');

d_log('Finishing...');
db::query('UPDATE `dump` SET `step` = 1 WHERE `date` = \''.$dump.'\'');
db::commit();

d_log('Finished!');

function d_log($message) {
    echo date('Y-m-d H:i:s').' - '.$message."\n";
}

function addElementToArray(&$array, $elements) {
    if (is_array($elements)) {
        foreach ($elements as $element) {
            $array[] = $element;
        }
    }
    if (is_string($elements)) {
        $array[] = $elements;
    }
}

?>