#!/bin/sh

# exit on error
set -e

# working directory
cd "$(dirname "$0")"

# execute scripts
java -jar denelezh-import.jar "$@"
php -f import.php
