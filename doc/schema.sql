/*

Note: to improve performance, all foreign keys are implicit. Don't try this at home.

*/

/* import tables */

DROP TABLE IF EXISTS `human`;
CREATE TABLE `human` (
  `id` INT UNSIGNED NOT NULL,
  `gender` INT UNSIGNED DEFAULT NULL,
  `birthyear` SMALLINT DEFAULT NULL,
  `sitelinks` SMALLINT NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `human_country`;
CREATE TABLE `human_country` (
  `human` INT UNSIGNED NOT NULL,
  `country` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`human`,`country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `human_occupation`;
CREATE TABLE `human_occupation` (
  `human` INT UNSIGNED NOT NULL,
  `occupation` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`human`,`occupation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `human_sitelink`;
CREATE TABLE `human_sitelink` (
  `human` INT UNSIGNED NOT NULL,
  `sitelink` VARCHAR(32) CHARACTER SET ascii NOT NULL,
  PRIMARY KEY (`human`,`sitelink`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `label`;
CREATE TABLE `label` (
  `id` INT UNSIGNED NOT NULL,
  `label` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `occupation_parent`;
CREATE TABLE `occupation_parent` (
  `occupation` INT UNSIGNED NOT NULL,
  `parent` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`occupation`,`parent`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/* reference tables */

DROP TABLE IF EXISTS `dump`;
CREATE TABLE `dump` (
  `date` DATE NOT NULL,
  `step` TINYINT NOT NULL,
  PRIMARY KEY (`date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `year`;
CREATE TABLE `year` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT,
  `year` SMALLINT NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `year_UNIQUE` (`year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `country`;
CREATE TABLE `country` (
  `id` INT UNSIGNED NOT NULL,
  `label` VARCHAR(500) DEFAULT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `occupation`;
CREATE TABLE `occupation` (
  `id` INT UNSIGNED NOT NULL,
  `label` VARCHAR(500) DEFAULT NULL,
  `date` DATE NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` SMALLINT NOT NULL AUTO_INCREMENT,
  `code` VARCHAR(32) CHARACTER SET ascii NOT NULL,
  `type` VARCHAR(32) DEFAULT NULL,
  `label` VARCHAR(128) DEFAULT NULL,
  `url` VARCHAR(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code_UNIQUE` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/* stats */

DROP TABLE IF EXISTS `kpi`;
CREATE TABLE `kpi` (
  `dump` DATE NOT NULL,
  `birthyear` SMALLINT NOT NULL,
  `country` INT UNSIGNED NOT NULL,
  `occupation` INT UNSIGNED NOT NULL,
  `project` SMALLINT NOT NULL,
  `humans` INT NOT NULL,
  `females` INT UNSIGNED NOT NULL,
  `males` INT UNSIGNED NOT NULL,
  `others` INT UNSIGNED NOT NULL,
  `humans_with_sitelink` INT NOT NULL,
  `females_with_sitelink` INT UNSIGNED NOT NULL,
  `males_with_sitelink` INT UNSIGNED NOT NULL,
  `others_with_sitelink` INT UNSIGNED NOT NULL,
  `humans_sitelinks` INT NOT NULL,
  `females_sitelinks` INT UNSIGNED NOT NULL,
  `males_sitelinks` INT UNSIGNED NOT NULL,
  `others_sitelinks` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`birthyear`,`country`,`occupation`,`project`,`dump`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4
PARTITION BY LIST COLUMNS(`dump`) (PARTITION `dump20010101` VALUES IN ('2001-01-01'));
